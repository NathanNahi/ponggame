var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var screens;
(function (screens) {
    var GameScreen = (function (_super) {
        __extends(GameScreen, _super);
        function GameScreen() {
            _super.call(this);
        }
        GameScreen.prototype.create = function () {
            this.game.sound.play("loop", 0.7, true);
            gameRoot.game.physics.startSystem(Phaser.Physics.ARCADE);
            gameRoot.game.stage.backgroundColor = "#FFFFFF";
            this.paddleController = new control.RadialFollower(0.1, 0, true);
            this.paddleController.applyVFollower(0.01);
            this.paddleController.onPolarLocation.add(this.startGame, this);
            var scoreStyle = { font: 'myriadpro', fontSize: '300px', fill: '#79D1CB' };
            this.score = 0;
            this.scoreText = gameRoot.game.add.text(gameRoot.game.width / 2, gameRoot.game.height / 2, "", scoreStyle);
            this.scoreText.anchor.setTo(0.5);
            var highscoreStyle = { font: 'myriadpro', fontSize: '130px', fill: '#79D1CB' };
            this.highscore = 0;
            this.highscoreText = gameRoot.game.add.text(gameRoot.game.width / 10 * 9, gameRoot.game.height / 10 * 1, "", highscoreStyle);
            this.highscoreText.setText(this.highscore.toString());
            this.highscoreText.anchor.setTo(0.5);
            this.paddle = new entities.Paddle(gameRoot.game.width / 2, gameRoot.game.height / 2, gameRoot.game.height / 2 - 50, (2 * Math.PI) / 4);
            this.ball = new entities.Ball(gameRoot.game.width / 2, gameRoot.game.height / 2, 30, 70, this.paddle);
            this.home = new gameUI.RingButton(gameRoot.game, gameRoot.game.width / 2, gameRoot.game.height / 2, 'icon_home', function () { console.log("Bloop"); }, this);
        };
        GameScreen.prototype.update = function () {
            this.paddleController.update(gameRoot.game.time.elapsed);
            this.paddle.rotation = -this.paddleController.theta;
            this.paddle.rotation = Utils.circleWrap(this.paddle.rotation);
            this.ball.rotation = Utils.circleWrap(this.ball.rotation);
            this.highscoreText.setText(this.highscore.toString());
            if (this.score > 0) {
                this.scoreText.setText(this.score.toString());
            }
        };
        GameScreen.prototype.startGame = function () {
            if (!this.ball.active) {
                this.ball.active = true;
            }
        };
        return GameScreen;
    }(Phaser.State));
    screens.GameScreen = GameScreen;
})(screens || (screens = {}));
var screens;
(function (screens) {
    var LoadingScreen = (function (_super) {
        __extends(LoadingScreen, _super);
        function LoadingScreen() {
            _super.call(this);
        }
        LoadingScreen.prototype.create = function () {
            console.log("Loading Screen reached");
            gameRoot.game.load.onLoadStart.add(this.onLoadStart, this);
            gameRoot.game.load.onLoadComplete.add(this.onLoadComplete, this);
            this.prepareLoadStack();
        };
        LoadingScreen.prototype.prepareLoadStack = function () {
            // Add the things ...
            gameRoot.game.load.image("icon_leaderboard", "assets/icon-leaderboard.png");
            gameRoot.game.load.image("icon_home", "assets/icon-home.png");
            gameRoot.game.load.image("button_play", "assets/button-play.png");
            gameRoot.game.load.audio("loop", "assets/sound/pongLoop.mp3", true);
            gameRoot.game.load.audio("note0", "assets/sound/pongC.mp3", true);
            gameRoot.game.load.audio("note1", "assets/sound/pongD.mp3", true);
            gameRoot.game.load.audio("note2", "assets/sound/pongE.mp3", true);
            gameRoot.game.load.audio("note3", "assets/sound/pongF.mp3", true);
            gameRoot.game.load.audio("note4", "assets/sound/pongG.mp3", true);
            gameRoot.game.load.audio("note5", "assets/sound/pongA.mp3", true);
            gameRoot.game.load.audio("note6", "assets/sound/pongB.mp3", true);
            gameRoot.game.load.start();
        };
        LoadingScreen.prototype.onLoadStart = function () {
            console.log("Loading started ...");
        };
        LoadingScreen.prototype.onLoadComplete = function () {
            console.log("Loading completed ...");
            gameRoot.game.state.start("game");
        };
        return LoadingScreen;
    }(Phaser.State));
    screens.LoadingScreen = LoadingScreen;
})(screens || (screens = {}));
var Utils;
(function (Utils) {
    function replaceAll(target, search, replacement) {
        return target.replace(new RegExp(search, 'g'), replacement);
    }
    Utils.replaceAll = replaceAll;
    function mod(a, b) {
        var m = a % b;
        if (m < 0) {
            return b + m;
        }
        else {
            return m;
        }
    }
    Utils.mod = mod;
    /**
     * The round topped gradual slope hill function takes two points and a scalar and returns a normalized scalar value
       that is 1 when the two points are equal and approaches 0 as they become seperated
     */
    function gaussian2d(mu, v, sigma) {
        if (sigma === void 0) { sigma = 1; }
        var d = psub(mu, v), dsq = Math.pow(d.m, 2), gfactor = Math.exp(-dsq / (2 * Math.pow(sigma, 2))) / (2 * Math.PI * Math.pow(sigma, 2));
        return gfactor;
    }
    Utils.gaussian2d = gaussian2d;
    /**
     * mode (v) relative to (mu) based of a scaled gaussian function
     * Dfactor is how much to move by when the point is exactly the mean
     * Sigma is the standard deviation, determining how far the distortion will spread
     */
    function gaussianDistortion(dFactor, v, mu, sigma) {
        if (sigma === void 0) { sigma = 1; }
        var gFactor = gaussian2d(mu, v, sigma), d = psub(mu, v), nDir = { x: d.x / d.m, y: d.y / d.m };
        return new Point(v.x + nDir.x * gFactor * dFactor, v.y + nDir.y * gFactor * dFactor);
    }
    Utils.gaussianDistortion = gaussianDistortion;
    /**
     * Subtract two points or vectors of the x,y object form, returning the differences and a magnitude
     * the args are (b, a) meaning the vector is a -> b or b-a
     */
    function psub(b, a) {
        var vx = b.x - a.x;
        var vy = b.y - a.y;
        var h = Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2));
        return { x: vx, y: vy, m: h };
    }
    Utils.psub = psub;
    /**
    *  linear interpolation of two xy points(p1,p2) with parameter (p)=1 -> p2 && (p) = 0 -> p1
    */
    function interpolate(p1, p2, p) {
        var p1_p2 = psub(p2, p1);
        return { x: p1.x + p1_p2.x * p, y: p1.y + p1_p2.y * p };
    }
    Utils.interpolate = interpolate;
    /** turn an angle and radial distance to an x distance */
    function polarToCartX(theta, radius) {
        if (radius === void 0) { radius = 1; }
        return Math.cos(theta) * radius;
    }
    Utils.polarToCartX = polarToCartX;
    /**turn an angle and radial distance to a y distance */
    function polarToCartY(theta, radius) {
        if (radius === void 0) { radius = 1; }
        return Math.sin(theta) * radius;
    }
    Utils.polarToCartY = polarToCartY;
    /** part of an attempt at stretching */
    // export function circleskew(theta:number):number{
    //     var thetaw = wrap(0, Math.PI/2, theta);
    //     return Math.sqrt(1+ Math.pow((1-Math.abs(1-4*thetaw/Math.PI))*Math.tan(thetaw), 2));
    // }
    function bradsToRads(brad) {
        return brad * Math.PI / 128;
    }
    Utils.bradsToRads = bradsToRads;
    /**
     * take an upper and lower bound and find the cyclically wrapped value of a number
       calculates by recursively adding or subtracting the upper-lower value so may not be efficient for large numbers
     */
    function wrap(lower, upper, num) {
        if (num < lower) {
            return wrap(lower, upper, num + (upper - lower));
        }
        else if (num > upper) {
            return wrap(lower, upper, num - (upper - lower));
        }
        else {
            return num;
        }
    }
    Utils.wrap = wrap;
    /**
     * calculate whether a number is within two angles going counterclockwise
     */
    function inCircleBound(num, lowAngle, highAngle) {
        var num = circleWrap(num);
        if (lowAngle <= highAngle) {
            return num > lowAngle && num < highAngle;
        }
        else {
            return (num > lowAngle && num > highAngle) || (num < lowAngle && num < highAngle);
        }
    }
    Utils.inCircleBound = inCircleBound;
    /**
     *Wrap withing the radian circle range
     */
    function circleWrap(theta) {
        return wrap(0, 2 * Math.PI, theta);
    }
    Utils.circleWrap = circleWrap;
    /** return the angle converted from Ac(anticlockwise) to Oc(clockwise)*/
    function angleComplement(theta) {
        return (theta < 0 ? circleWrap(theta) : -(circleWrap(-theta)));
    }
    Utils.angleComplement = angleComplement;
    /** return the shortest way to get from one angle to another around a radian circle */
    function shortAngle(angFrom, angTo) {
        var a = circleWrap(angFrom);
        var b = circleWrap(angTo);
        var d = b - a;
        if (-Math.PI <= d && d < Math.PI) {
            return d;
        }
        else {
            //over half the circle away(complement direction)
            console.log("a: " + a + "  b: " + b + "  angleComplement(b-a)" + angleComplement(d));
            return angleComplement(d);
        }
    }
    Utils.shortAngle = shortAngle;
    ;
    function circleDistance(ang1, ang2) {
        return Math.abs(shortAngle(ang1, ang2));
    }
    Utils.circleDistance = circleDistance;
    /**
     * A point class fundamentally in the cartesian plane but providing conversions to polar space
     */
    var Point = (function () {
        function Point(x, y) {
            this.x = x;
            this.y = y;
        }
        Point.prototype.polarTo = function (x, y) {
            var xrel = x - this.x;
            var yrel = y - this.y;
            var atan2 = Math.atan2(yrel, xrel);
            if (atan2 < 0) {
                atan2 += 2 * Math.PI;
            }
            return { r: Math.sqrt(xrel * xrel + yrel * yrel), theta: atan2 };
        };
        Point.prototype.cartesianTo = function (r, theta) {
            return {
                x: this.x + r * Math.cos(theta),
                y: this.y + r * Math.sin(theta)
            };
        };
        Point.prototype.polarFrom = function (x, y) {
            var xrel = this.x - x;
            var yrel = this.y - y;
            var atan2 = Math.atan2(yrel, xrel);
            if (atan2 < 0) {
                atan2 += 2 * Math.PI;
            }
            return { r: Math.sqrt(xrel * xrel + yrel * yrel), theta: atan2 };
        };
        Point.prototype.polarFromOrigin = function () {
            return this.polarFrom(0, 0);
        };
        Point.prototype.polarToOrigin = function () {
            return this.polarTo(0, 0);
        };
        return Point;
    }());
    Utils.Point = Point;
    function radialPoint(angle, magnitude) {
        return new Point(polarToCartX(angle, magnitude), polarToCartY(angle, magnitude));
    }
    Utils.radialPoint = radialPoint;
    /**
    this class represents a context of point interpretation, used by the tgoma game-pads framework to convert trampoline coordinates

    the scene performs mappings from radial to cartesian coordinates within windows of different sizes.

    the mapping methods are:
        radialToCart:square fits around circle snugly, keeping
        radialToCartClip:circle fits around square, thus clipping some of the input to the edge of the square
        radialToCartStretch: fill a square with a circle by linearly stretching the radius out to the bounding square
        radialToCartMap: approximate stretch by

    */
    var Scene = (function () {
        function Scene(description) {
            this.cx = description.w / 2;
            this.cy = description.h / 2;
            this.x = description.x;
            this.y = description.y;
        }
        Scene.prototype.radialToCart = function (rn, angle) {
            var _x = this.cx + this.x + Math.cos(angle) * rn * this.cx;
            var _y = this.cy + this.y - Math.sin(angle) * rn * this.cy;
            return new Point(_x, _y);
        };
        Scene.prototype.radialToCartClip = function (rn, angle) {
            var rx = Math.cos(angle) * rn * Math.SQRT2;
            var ry = Math.sin(angle) * rn * Math.SQRT2;
            rx = rx > 0 ? Math.min(1, rx) : Math.max(-1, rx);
            ry = ry > 0 ? Math.min(1, ry) : Math.max(-1, ry);
            var _x = this.cx + this.x + rx * this.cx;
            var _y = this.cy + this.y - ry * this.cy;
            return new Point(_x, _y);
        };
        /**
         * Fill a square with a circular input space.
         */
        Scene.prototype.radialToCartStretch = function (rn, angle) {
            var rx, ry;
            var m = mod(Math.floor(2 * (angle + Math.PI / 4) / Math.PI), 2);
            var d;
            if (m == 0) {
                d = 1 / Math.cos(angle);
            }
            else if (m == 1) {
                d = 1 / Math.sin(angle);
            }
            d = Math.abs(d);
            var _x = this.cx + this.x + rn * d * Math.cos(angle) * this.cx;
            var _y = this.cy + this.y - rn * d * Math.sin(angle) * this.cy;
            return new Point(_x, _y);
        };
        Scene.prototype.radialToCartMap = function (polar_magnitude, polar_angle) {
            var x = Math.cos(polar_angle) * polar_magnitude;
            var y = Math.sin(polar_angle) * polar_magnitude;
            var _x = x * Math.sqrt(1 - (Math.pow(y, 2) / 2));
            var _y = y * Math.sqrt(1 - Math.pow(x, 2) / 2);
            var _x = this.cx + this.x + _x * this.cx;
            var _y = this.cy + this.y - _y * this.cy;
            return new Point(_x, _y);
        };
        return Scene;
    }());
    Utils.Scene = Scene;
    function getPosition(element) {
        var xPosition = 0;
        var yPosition = 0;
        while (element) {
            xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
            yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
            element = element.offsetParent;
        }
        return { x: xPosition, y: yPosition };
    }
    Utils.getPosition = getPosition;
    function mouseDebug(e) {
        var targetPos = getPosition(e.target);
        var p = new Utils.Point((e.pageX - targetPos.x), e.pageY - targetPos.y);
        var polarexp = p.polarFromOrigin();
        console.log("client x: " + e.clientX
            + "\n client y:" + e.clientY
            + "\n screen x: " + e.screenX
            + "\n screen y: " + e.screenY
            + "\n page x: " + e.pageX
            + "\n page y: " + e.pageY
            + "\n offset left: " + e.target.offsetLeft
            + "\n offset top: " + e.target.offsetTop
            + "\n point x" + (e.pageX - targetPos.x)
            + "\n point y" + (e.pageY - targetPos.y)
            + "\n polarAngle:" + polarexp.theta
            + "\n polarMag: " + polarexp.r);
    }
    Utils.mouseDebug = mouseDebug;
    function DumpObjectIndented(obj, indent) {
        var result = "";
        if (indent == null)
            indent = "";
        for (var property in obj) {
            var value = obj[property];
            if (typeof value == 'string')
                value = "'" + value + "'";
            else if (typeof value == 'object') {
                if (value instanceof Array) {
                    // Just let JS convert the Array to a string!
                    value = "[ " + value + " ]";
                }
                else {
                    // Recursive dump
                    // (replace "  " by "\t" or something else if you prefer)
                    var od = DumpObjectIndented(value, indent + "  ");
                    // If you like { on the same line as the key
                    //value = "{\n" + od + "\n" + indent + "}";
                    // If you prefer { and } to be aligned
                    value = "\n" + indent + "{\n" + od + "\n" + indent + "}";
                }
            }
            result += indent + "'" + property + "' : " + value + ",\n";
        }
        return result.replace(/,\n$/, "");
    }
    Utils.DumpObjectIndented = DumpObjectIndented;
    /**
     * An expression of the variable that
     */
    var BracketedWatcher = (function () {
        function BracketedWatcher(watch, brackets) {
            this.watch = watch;
            this.brackets = brackets;
            this.goingUp = brackets[1] > brackets[0];
            this.bracketIndex = this.goingUp ? 0 : this.brackets.length;
        }
        BracketedWatcher.prototype.update = function () {
            var sigma = this.watch.getBracketVariable();
            var dir = this.goingUp ? 1 : -1;
            var bi = this.bracketIndex;
            var len = this.brackets.length;
            //find bi such that
            //upwards v[bi] <= sigma < v[bi+1]
            //downwards v[bi] >= sigma < v[bi+1]
            if (this.goingUp) {
                while (bi < len && sigma >= this.brackets[bi]) {
                    bi += 1;
                }
                while (bi > 0 && sigma < this.brackets[bi - 1]) {
                    bi -= 1;
                }
            }
            else {
                while (bi < len && sigma <= this.brackets[bi]) {
                    bi += 1;
                }
                while (bi > 0 && sigma > this.brackets[bi - 1]) {
                    bi -= 1;
                }
            }
            this.bracketIndex = bi;
        };
        return BracketedWatcher;
    }());
    Utils.BracketedWatcher = BracketedWatcher;
    function translator(node, translation) {
        var translated;
        //array?
        if (typeof (node) == "object" && !(node instanceof Array)) {
            translated = {};
            for (var k in node) {
                var tval = translation[k];
                if (typeof (tval) == "function") {
                    //rename to the function name with function value
                    translated[tval.name] = tval(node[k]);
                }
                if (typeof (tval) == "string") {
                    //rename the leaf
                    translated[tval] = node[k];
                }
                else if (tval != undefined) {
                    translated[k] = translator(node[k], tval);
                }
                else {
                    //dont bother recurring if the translator wont come
                    translated[k] = node[k];
                }
            }
            return translated;
        }
        else {
            return node;
        }
    }
    Utils.translator = translator;
    var t = { a: 2, b: { c: 3, d: 4 } };
    var ft = { a: function r(x) { return x + 1; }, b: { c: function c(x) { return x - 1; } } };
    var l = translator(t, ft);
    function melder(node1, node2, merge, concatArrays) {
        if (merge === void 0) { merge = function (a, b) { return b; }; }
        if (concatArrays === void 0) { concatArrays = false; }
        if (node1 == undefined) {
            return node2;
        }
        if (node2 == undefined) {
            return node1;
        }
        if (typeof (node1) != typeof (node2)) {
            var errmsg = "Expected melding nodes to be the same type \n" +
                "type of node1: " + typeof (node1) + "\n" +
                "type of node2: " + typeof (node2) + "\n";
            throw TypeError(errmsg);
        }
        var melded;
        if (node1 instanceof Array) {
            melded = concatArrays ? node1.concat(node2) : merge(node1, node2);
        }
        else if (typeof (node1) == 'object') {
            melded = {};
            //in one or the other
            for (var k in node1) {
                melded[k] = node1[k];
            }
            for (var q in node2) {
                melded[q] = node2[q];
            }
            //in both
            for (var k in node1) {
                for (var q in node2) {
                    if (k == q) {
                        melded[k] = melder(node1[k], node2[k], merge, concatArrays);
                    }
                }
            }
        }
        else {
            // if they are not objects just take the second argument
            melded = merge(node1, node2);
        }
        return melded;
    }
    Utils.melder = melder;
    //merge, when there is a conflict, neither is taken
    function softAssoc(from, onto) {
        for (var k in from) {
            onto[k] = melder(from[k], onto[k]);
        }
    }
    Utils.softAssoc = softAssoc;
    function assoc(from, onto) {
        for (var k in from) {
            onto[k] = melder(onto[k], from[k]);
        }
    }
    Utils.assoc = assoc;
    function copyObject(object) {
        var cp = {};
        Utils.assoc(object, cp);
        return cp;
    }
    Utils.copyObject = copyObject;
    function applyMixins(derivedCtor, baseCtors) {
        baseCtors.forEach(function (baseCtor) {
            Object.getOwnPropertyNames(baseCtor.prototype).forEach(function (name) {
                derivedCtor.prototype[name] = baseCtor.prototype[name];
            });
        });
    }
    Utils.applyMixins = applyMixins;
    function thingize(thingsOrThing) {
        var thing;
        if (thingsOrThing instanceof Array) {
            thing = thingsOrThing[Math.floor(Math.random() * thingsOrThing.length)];
        }
        else {
            thing = thingsOrThing;
        }
        return thing;
    }
    Utils.thingize = thingize;
})(Utils || (Utils = {}));
/// <reference path="../tsdefs/Tgoma.d.ts"/>
/// <reference path="../utils.ts"/>
var control;
(function (control) {
    /**
     * all pads subscribe their polar location to the tgoma signal
     */
    var BaseGamePad = (function () {
        function BaseGamePad(normalized) {
            this.normalized = normalized;
            this.enabled = true;
            this.rateFollow = false;
            this.timemark = new Date().getTime();
            this.rateCoeff = 1;
            try {
                if (normalized) {
                    this.onPolarLocation = Tgoma.TrampolineHelper.polarLocationGameCircleNormalSignal;
                }
                else {
                    this.onPolarLocation = Tgoma.TrampolineHelper.polarLocationMaximumAreaSignal;
                }
                Tgoma.TrampolineHelper.subscribeToMessage(TgomaMessaging.Enum.TrampolineMessages.ID_EVENT_IMPACT, this._onImpact, this);
                Tgoma.TrampolineHelper.subscribeToMessage(TgomaMessaging.Enum.TrampolineMessages.ID_EVENT_DEPART, this._onDepart, this);
            }
            catch (exception) {
                console.error("cant access Tgoma Libarary");
                this.onPolarLocation = new Signal();
            }
            this.onImpact = new Signal();
            this.onDepart = new Signal();
        }
        BaseGamePad.prototype.addPolarLocationListener = function (onPolarLocation) {
            this.onPolarLocation.add(onPolarLocation);
        };
        BaseGamePad.prototype.addImpactListener = function (onImpact) {
            this.onImpact.add(onImpact);
        };
        BaseGamePad.prototype.addDepartListener = function (onDepart) {
            this.onImpact.add(onDepart);
        };
        BaseGamePad.prototype.activateRateFollowing = function (followCoeff) {
            this.rateCoeff = followCoeff;
            this.rateFollow = true;
            this.rate = 0;
        };
        BaseGamePad.prototype.teardown = function () {
            this.onPolarLocation.removeAll();
            Tgoma.TrampolineHelper.unsubscribeFromMessage(TgomaMessaging.Enum.TrampolineMessages.ID_EVENT_IMPACT, this._onImpact, this);
            Tgoma.TrampolineHelper.unsubscribeFromMessage(TgomaMessaging.Enum.TrampolineMessages.ID_EVENT_DEPART, this._onDepart, this);
        };
        BaseGamePad.prototype.enable = function () {
            this.enabled = true;
            this.onPolarLocation.active = true;
        };
        BaseGamePad.prototype.disable = function () {
            this.enabled = false;
            this.onPolarLocation.active = false;
        };
        BaseGamePad.prototype._onImpact = function (airtime) {
            if (!this.enabled) {
                return;
            }
            this.onImpact.dispatch(airtime);
            var t = new Date().getTime();
            var delt = t - this.timemark;
            if (this.rateFollow) {
                this.rate = this.rate * (1 - this.rateCoeff) + this.rateCoeff / delt;
                this.timemark = t;
            }
            if (this.nmin && this.nmax) {
                this.normalRate = (this.rate - this.nmin) / (this.nmax - this.nmax);
            }
        };
        BaseGamePad.prototype._onDepart = function (mattime) {
            if (!this.enabled) {
                return;
            }
            this.onDepart.dispatch(mattime);
        };
        return BaseGamePad;
    }());
    var GamePadAdaptor = (function (_super) {
        __extends(GamePadAdaptor, _super);
        function GamePadAdaptor(normalized, controller) {
            this.onImpact.add(controller.onImpact);
            this.onDepart.add(controller.onDepart);
            this.onPolarLocation.add(controller.onPolarLocation);
            this.core = controller;
            _super.call(this, normalized);
        }
        return GamePadAdaptor;
    }(BaseGamePad));
    (function (SquareInputTypes) {
        SquareInputTypes[SquareInputTypes["KEEP"] = 0] = "KEEP";
        SquareInputTypes[SquareInputTypes["CLIP"] = 1] = "CLIP";
        SquareInputTypes[SquareInputTypes["STRETCH"] = 2] = "STRETCH";
        SquareInputTypes[SquareInputTypes["MAP"] = 3] = "MAP";
    })(control.SquareInputTypes || (control.SquareInputTypes = {}));
    var SquareInputTypes = control.SquareInputTypes;
    control.directionalSquareInput = {
        x: -1, y: -1, w: 2, h: 2
    };
    /**
       A game controller that converts tgoma input into cartesian coordinates
       @param sceneDescription:Utils.SceneDescription={x:0,y:0,w:1,h:1} an object with x,y,w,h to deterine the output scope of the coordinates eg {x:0, y:0, w:gameWidth, h:gameHeight} would map to the screen
       @param callback: the function called on each polar location event
       @param conversion type one of:
           SquareInputTypes.KEEP : The polar circle sits snugly within the output square
           SquareInputTypes.CLIP : the polar circle sits around the output square limiting the outer reaches to the square border
           SquareInputTypes.STRETCH : the polar circle stretches to the edges of the output square hence distorting the input space but allowing the full range
    */
    var SquareInputPad = (function (_super) {
        __extends(SquareInputPad, _super);
        function SquareInputPad(sceneDescription, conversionType) {
            if (sceneDescription === void 0) { sceneDescription = { x: 0, y: 0, w: 1, h: 1 }; }
            if (conversionType === void 0) { conversionType = SquareInputTypes.KEEP; }
            _super.call(this, true);
            this.sceneDescription = sceneDescription;
            this.conversionType = conversionType;
            this.squareLocationListeners = [];
            this.preclipping = false;
            this.postclipping = false;
            this.preclippingBounds = { xmin: 0, xmax: 1, ymin: 0, ymax: 1 };
            this.postclippingBounds = { xmin: 0, xmax: 1, ymin: 0, ymax: 1 };
            this.scene = new Utils.Scene({ x: 0, y: 0, w: 1, h: 1 });
            this.onSquareLocation = new Signal();
            this.onPolarLocation.add(this.onPolarLocationSquaring, this);
        }
        SquareInputPad.prototype.addSquareLocationListener = function (onSquareLocation) {
            this.onSquareLocation.add(onSquareLocation);
        };
        SquareInputPad.prototype.applyClip = function (clippingBox, boundsObject) {
            boundsObject.xmin = clippingBox.x;
            boundsObject.xmax = clippingBox.x + clippingBox.w;
            boundsObject.ymin = clippingBox.y;
            boundsObject.ymax = clippingBox.y + clippingBox.h;
        };
        SquareInputPad.prototype.applyPreClip = function (clippingBox) {
            this.applyClip(clippingBox, this.preclippingBounds);
            this.preclipping = true;
        };
        SquareInputPad.prototype.applyPostClip = function (clippingBox) {
            this.applyClip(clippingBox, this.postclippingBounds);
            this.postclipping = true;
        };
        SquareInputPad.prototype.clip = function (cart, bounds) {
            var clipped = {
                x: Math.min(bounds.xmax, Math.max(bounds.xmin, cart.x)),
                y: Math.min(bounds.ymax, Math.max(bounds.ymin, cart.y))
            };
            return clipped;
        };
        SquareInputPad.prototype.modalPolarLocation = function (polar_magnitude, polar_angle) {
            var cart;
            switch (this.conversionType) {
                case (SquareInputTypes.KEEP):
                    cart = this.scene.radialToCart(polar_magnitude, polar_angle);
                    break;
                case (SquareInputTypes.CLIP):
                    cart = this.scene.radialToCartClip(polar_magnitude, polar_angle);
                    break;
                case (SquareInputTypes.STRETCH):
                    cart = this.scene.radialToCartStretch(polar_magnitude, polar_angle);
                    break;
                case (SquareInputTypes.MAP):
                    cart = this.scene.radialToCartMap(polar_magnitude, polar_angle);
                    break;
            }
            return cart;
        };
        SquareInputPad.prototype.onPolarLocationSquaring = function (polar_magnitude, polar_angle) {
            var cart = this.modalPolarLocation(polar_magnitude, polar_angle);
            if (this.preclipping) {
                cart = this.clip(cart, this.preclippingBounds);
                //renormalize
                cart = {
                    x: (cart.x - this.preclippingBounds.xmin) / (this.preclippingBounds.xmax - this.preclippingBounds.xmin),
                    y: (cart.y - this.preclippingBounds.ymin) / (this.preclippingBounds.ymax - this.preclippingBounds.ymin)
                };
            }
            cart = {
                x: this.sceneDescription.x + this.sceneDescription.w * cart.x,
                y: this.sceneDescription.y + this.sceneDescription.h * cart.y
            };
            if (this.postclipping) {
                cart = this.clip(cart, this.postclippingBounds);
            }
            this.onSquareLocation.dispatch(cart);
        };
        SquareInputPad.prototype.teardown = function () {
            _super.prototype.teardown.call(this);
        };
        return SquareInputPad;
    }(BaseGamePad));
    control.SquareInputPad = SquareInputPad;
    var DifferencingPad = (function (_super) {
        __extends(DifferencingPad, _super);
        function DifferencingPad(sceneDescription, conversionType) {
            if (sceneDescription === void 0) { sceneDescription = { x: -1, y: -1, w: 2, h: 2 }; }
            if (conversionType === void 0) { conversionType = SquareInputTypes.KEEP; }
            _super.call(this, sceneDescription, conversionType);
            this.toRelativeChangeRate = 1;
            this.toAbsoluteChangeRate = 1;
            this.smark = 0;
            this.pmark = { x: 0, y: 0 };
            this.spaceCrossingNormalizer = Math.sqrt(Math.pow(this.sceneDescription.w, 2) + Math.pow(this.sceneDescription.h, 2));
            this.onDifferenced = new Signal();
            this.onMixed = new Signal();
            this.onSquareLocation.add(this.polarSquareIn, this);
        }
        DifferencingPad.prototype.polarSquareIn = function (cart) {
            var diff = Utils.psub(cart, this.pmark);
            var sIn = diff.m / this.spaceCrossingNormalizer;
            var sChange = (sIn < this.smark ? this.toAbsoluteChangeRate : this.toRelativeChangeRate);
            var sActual = sIn * sChange + this.smark * (1 - sChange);
            var mixed = { x: diff.x * sActual + cart.x * (1 - sActual),
                y: diff.y * sActual + cart.y * (1 - sActual) };
            this.onDifferenced.dispatch({ x: diff.x, y: diff.y });
            this.onMixed.dispatch(mixed);
        };
        return DifferencingPad;
    }(SquareInputPad));
    control.DifferencingPad = DifferencingPad;
    /**
     * store the values coming from the controller and allow application of PID effects
     */
    var XYFollowerPad = (function (_super) {
        __extends(XYFollowerPad, _super);
        function XYFollowerPad(sceneDescription, initialPosition, conversionType, vcap) {
            if (sceneDescription === void 0) { sceneDescription = { x: 0, y: 0, w: 1, h: 1 }; }
            if (initialPosition === void 0) { initialPosition = { x: 0, y: 0 }; }
            if (conversionType === void 0) { conversionType = SquareInputTypes.KEEP; }
            if (vcap === void 0) { vcap = -1; }
            this.xin = { position: initialPosition.x };
            this.yin = { position: initialPosition.y };
            this.x = initialPosition.x;
            this.y = initialPosition.y;
            this.sceneDescription = sceneDescription;
            this.vcap = vcap;
            _super.call(this, sceneDescription, conversionType);
            this.onSquareLocation.add(this.squareIn, this);
        }
        XYFollowerPad.prototype.squareIn = function (squareCoordinate) {
            this.xin.position = squareCoordinate.x;
            this.yin.position = squareCoordinate.y;
        };
        XYFollowerPad.prototype.applyVFollower = function (dvfactor) {
            this.xreflector = new VFollower(this.xin, dvfactor / this.sceneDescription.w, this.vcap);
            this.yreflector = new VFollower(this.yin, dvfactor / this.sceneDescription.h, this.vcap);
        };
        XYFollowerPad.prototype.applyAFollower = function (dafactor, vafactor) {
            this.xreflector = new AFollower(this.xin, dafactor / this.sceneDescription.w, vafactor / this.sceneDescription.w, this.vcap);
            this.yreflector = new AFollower(this.yin, dafactor / this.sceneDescription.h, vafactor / this.sceneDescription.h, this.vcap);
        };
        XYFollowerPad.prototype.reset = function (initialPosition) {
            this.xin = { position: initialPosition.x };
            this.yin = { position: initialPosition.y };
            this.x = initialPosition.x;
            this.y = initialPosition.y;
        };
        XYFollowerPad.prototype.update = function (dt) {
            if (this.xreflector != undefined) {
                this.xreflector.update(dt);
                this.yreflector.update(dt);
                this.x = this.xreflector.position;
                this.y = this.yreflector.position;
            }
            else {
                this.x = this.xin.position;
                this.y = this.yin.position;
            }
        };
        return XYFollowerPad;
    }(SquareInputPad));
    control.XYFollowerPad = XYFollowerPad;
    /**
     * a reverse facing linear reflection device
     */
    var PFollower = (function () {
        function PFollower(source, obj, prop, a, b) {
            this.source = source;
            this.obj = obj;
            this.prop = prop;
            this.position = 0;
            if (b != undefined) {
                this.a = a;
                this.b = b;
            }
            else {
                this.b = a;
                this.a = 0;
            }
        }
        PFollower.prototype.update = function () {
            var input = this.source.position;
            this.position = this.a + input * (this.b - this.a);
            //Reflect
            this.obj[this.prop] = this.position;
        };
        return PFollower;
    }());
    control.PFollower = PFollower;
    var VFollower = (function () {
        function VFollower(source, dvfactor, vcap) {
            if (vcap === void 0) { vcap = -1; }
            this.source = source;
            this.dvfactor = dvfactor;
            this.vcap = vcap;
            this.velocity = 0;
            this.position = source.position;
        }
        VFollower.prototype.update = function (dt) {
            var diff = this.source.position - this.position;
            this.velocity = diff * this.dvfactor;
            //  var vm = Math.abs(this.velocity);
            //  var vmr =Math.min(vm, this.vcap)
            //  this.velocity = vmr*this.velocity/vm
            this.position += dt * this.velocity;
        };
        return VFollower;
    }());
    control.VFollower = VFollower;
    var AFollower = (function () {
        function AFollower(source, dafactor, vafactor, vcap) {
            if (vcap === void 0) { vcap = -1; }
            this.source = source;
            this.dafactor = dafactor;
            this.vafactor = vafactor;
            this.vcap = vcap;
            this.velocity = 0;
            this.acceleration = 0;
            this.position = source.position;
        }
        AFollower.prototype.update = function (dt) {
            var diff = this.source.position - this.position;
            this.acceleration = diff * this.dafactor + this.velocity * this.vafactor;
            this.velocity += dt * this.acceleration;
            //
            //  var vm = Math.abs(this.velocity);
            //  var vmr =Math.min(vm, this.vcap)
            //  this.velocity = vmr*this.velocity/vm
            this.position += dt * this.velocity;
        };
        return AFollower;
    }());
    control.AFollower = AFollower;
    var SectorPad = (function (_super) {
        __extends(SectorPad, _super);
        function SectorPad(centerSize, sectors, startAngle) {
            if (startAngle === void 0) { startAngle = 0; }
            _super.call(this, true);
            this.centerSize = centerSize;
            this.sectors = sectors;
            this.startAngle = startAngle;
            this.callbacks = [];
            this.addPolarLocationListener(this.polarToSector.bind(this));
        }
        SectorPad.prototype.polarToSector = function (polar_magnitude, polar_angle) {
            if (polar_magnitude > this.centerSize) {
                var polar_angleN = Utils.circleWrap(polar_angle - this.startAngle) / (2 * Math.PI);
                var sector = Utils.mod(Math.floor(this.sectors * polar_angleN), this.sectors);
                this.dispatch(sector);
                console.log("[Sector Pad] normal_polar_angle: %f - sector: %d", polar_angleN, sector);
            }
        };
        SectorPad.prototype.dispatch = function (direction) {
            this.callbacks.forEach(function (value, index, array) {
                value(direction);
            });
        };
        SectorPad.prototype.onDirection = function (callback) {
            this.callbacks.push(callback);
        };
        return SectorPad;
    }(BaseGamePad));
    control.SectorPad = SectorPad;
    var RadialFollower = (function (_super) {
        __extends(RadialFollower, _super);
        function RadialFollower(centerSize, initialAngle, normalized) {
            if (normalized === void 0) { normalized = true; }
            _super.call(this, normalized);
            this.centerSize = centerSize;
            this.thetaTarget = initialAngle;
            this.theta = initialAngle;
            this.onCenterHit = new Signal();
            this.onPolarLocation.add(this.polarLocationIn, this);
        }
        RadialFollower.prototype.applyVFollower = function (dvfactor) {
            this.followStyle = "v";
            this.dv = dvfactor;
        };
        RadialFollower.prototype.applyAFollower = function (dafactor, vafactor) {
            this.followStyle = "a";
            this.da = dafactor;
            this.va = vafactor;
        };
        RadialFollower.prototype.polarLocationIn = function (r, theta) {
            if (r > this.centerSize) {
                this.r = r;
                this.thetaTarget = theta;
            }
        };
        RadialFollower.prototype.reset = function (initialPosition) {
            this.r = initialPosition.r;
            this.theta = initialPosition.theta;
        };
        RadialFollower.prototype.update = function (dt) {
            switch (this.followStyle) {
                case "v": {
                    var dTheta = Utils.shortAngle(this.theta, this.thetaTarget);
                    this.thetaV = dTheta * this.dv;
                    this.theta += this.thetaV * dt;
                    break;
                }
                case "a": {
                    var dTheta = Utils.shortAngle(this.theta, this.thetaTarget);
                    this.thetaA = dTheta * this.da + this.thetaV * this.va;
                    this.thetaV += dt * this.thetaA;
                    this.theta += this.thetaV * dt;
                    break;
                }
                default: {
                    this.theta = this.thetaTarget;
                }
            }
        };
        return RadialFollower;
    }(BaseGamePad));
    control.RadialFollower = RadialFollower;
    var SectoredRadialFollower = (function (_super) {
        __extends(SectoredRadialFollower, _super);
        function SectoredRadialFollower(centerSize, initialAngle, sectors, startAngle, snapping, normalized) {
            if (normalized === void 0) { normalized = true; }
            _super.call(this, centerSize, initialAngle, normalized);
            this.sectors = sectors;
            this.startAngle = startAngle;
            this.snapping = snapping;
            this.sectorSize = (2 * Math.PI / this.sectors);
            this.onSectorHit = new Signal();
            this.onSectorReached = new Signal();
        }
        SectoredRadialFollower.prototype.getSectorAt = function (theta) {
            var polar_angleN = Utils.circleWrap(theta - this.startAngle) / (2 * Math.PI);
            return Utils.mod(Math.floor(this.sectors * polar_angleN), this.sectors);
        };
        SectoredRadialFollower.prototype.polarLocationIn = function (theta, r) {
            this.r = r;
            var sector = this.getSectorAt(theta);
            if (r > this.centerSize) {
                this.onSectorHit.dispatch(sector);
            }
            else {
                this.onCenterHit.dispatch();
            }
            if (this.snapping) {
                this.thetaTarget = this.getTargetTheta(theta, sector);
            }
            else {
                this.thetaTarget = theta;
            }
        };
        SectoredRadialFollower.prototype.getTargetTheta = function (theta, sector) {
            return this.startAngle + (sector + 1 / 2) * (2 * Math.PI / this.sectors);
        };
        SectoredRadialFollower.prototype.update = function () {
            RadialFollower.prototype.update.call(this);
            var sector = this.getSectorAt(this.theta);
            if (sector != this.sector) {
                this.onSectorReached.dispatch(sector, this.sector);
                this.sector = sector;
            }
        };
        return SectoredRadialFollower;
    }(RadialFollower));
    control.SectoredRadialFollower = SectoredRadialFollower;
    var DarkSectorRadialFollower = (function (_super) {
        __extends(DarkSectorRadialFollower, _super);
        function DarkSectorRadialFollower(centerSize, initialAngle, sectors, startAngle, outreach, normalized) {
            if (outreach === void 0) { outreach = 0.5; }
            if (normalized === void 0) { normalized = true; }
            _super.call(this, centerSize, initialAngle, sectors, startAngle, true, normalized);
            this.activationStates = [];
            this.reachSize = (1 + outreach) * this.sectorSize;
            for (var i = 0; i < sectors; i++) {
                this.activationStates[i] = true;
            }
        }
        DarkSectorRadialFollower.prototype.getTargetTheta = function (theta, sector) {
            var here = this.activationStates[sector];
            var htheta = this.startAngle + (sector + 1 / 2) * this.sectorSize;
            if (here) {
                return htheta;
            }
            else {
                var Ac = this.activationStates[Utils.mod(sector + 1, this.sectors)];
                var Oc = this.activationStates[Utils.mod(sector - 1, this.sectors)];
                var actheta = htheta + this.sectorSize;
                var octheta = htheta - this.sectorSize;
                var dir = Utils.shortAngle(theta, htheta);
                var toOc = Math.abs(Utils.shortAngle(theta, octheta));
                var toAc = Math.abs(Utils.shortAngle(theta, actheta));
                var closerNeighbor = (toAc <= toOc ? actheta : octheta);
                if (Ac && !Oc && toAc < this.reachSize) {
                    return actheta;
                }
                else if (!Ac && Oc && toOc < this.reachSize) {
                    return octheta;
                }
                else if (Ac && Oc && closerNeighbor < this.reachSize) {
                    return closerNeighbor;
                }
                else {
                    return theta;
                }
            }
        };
        return DarkSectorRadialFollower;
    }(SectoredRadialFollower));
    control.DarkSectorRadialFollower = DarkSectorRadialFollower;
    /**
      A basic 4 way controller linking to trampoline
     either set the buttons individullly:
      var ctl = control.DPad(0.5)
      ctl.onUp = function(){
           player.vx = 0;
           player.vy = 1;
       }

     Or provide a target object

     var gameActuator = {
         onRight: function(){player.vx = 1; player.vy = 0};
         onLeft:  function(){player.vx = -1; player.vy = 0};
         onUp:    function(){player.vx = 0; player.vy = -1};
         onDown:  function(){player.vx = 0; player.vy = 1};
         onCenter:function(){player.vx = 0; player.vy = 0};
     }

     var ctl = control.DPad(0.5, gameActuator)

     and change the target with

     ctl.target(gameActuator)

     @param centerSize: A number between 0 and 1 indicating how far out input becomes directional
     */
    var DPad = (function (_super) {
        __extends(DPad, _super);
        function DPad(centerSize, target) {
            _super.call(this, true);
            this.centerSize = centerSize;
            this.dirIndex = {};
            this.dirIndex[0] = function () {
                console.log("game-pads.ts DPad onUp");
            };
            this.dirIndex[1] = function () {
                console.log("game-pads.ts DPad onUp");
            };
            this.dirIndex[2] = function () {
                console.log("game-pads.ts DPad onUp");
            };
            this.dirIndex[3] = function () {
                console.log("game-pads.ts DPad onUp");
            };
            this.dirIndex[5] = function () {
            };
            if (target) {
                this.target(target);
            }
            this.onPolarLocation.add(this._onPolarLocation, this);
        }
        DPad.prototype._onPolarLocation = function (polar_magnitude, polar_angle) {
            if (polar_magnitude > this.centerSize) {
                var polar_angleN = (Math.PI / 2) - polar_angle;
                var sector = Utils.mod(Math.floor(2 * (Math.PI * 3 / 4 - polar_angle) / Math.PI), 4);
                this.dirIndex[sector]();
            }
            else {
                this.dirIndex[5]();
            }
        };
        /**
         *
         */
        DPad.prototype.target = function (dpadTarget) {
            this.dirIndex[0] = function () {
                dpadTarget.onUp();
            };
            this.dirIndex[1] = function () {
                dpadTarget.onRight();
            };
            this.dirIndex[2] = function () {
                dpadTarget.onDown();
            };
            this.dirIndex[3] = function () {
                dpadTarget.onLeft();
            };
            this.dirIndex[5] = function () {
                dpadTarget.onCenter();
            };
        };
        Object.defineProperty(DPad.prototype, "onUp", {
            set: function (value) {
                this.dirIndex[0] = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DPad.prototype, "onRight", {
            set: function (value) {
                this.dirIndex[1] = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DPad.prototype, "onDown", {
            set: function (value) {
                this.dirIndex[2] = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DPad.prototype, "onLeft", {
            set: function (value) {
                this.dirIndex[3] = value;
            },
            enumerable: true,
            configurable: true
        });
        return DPad;
    }(BaseGamePad));
    control.DPad = DPad;
})(control || (control = {}));
var exports = exports || {};
exports.control = control;
var Zonerism;
(function (Zonerism) {
    var EmptyZone = (function () {
        function EmptyZone() {
            this.contains = function (x, y) {
                return false;
            };
        }
        return EmptyZone;
    }());
    Zonerism.EmptyZone = EmptyZone;
    var RectangleZone = (function (_super) {
        __extends(RectangleZone, _super);
        function RectangleZone(x, y, w, h, data) {
            var _this = this;
            _super.call(this);
            this.contains = function (x, y) {
                if (x >= _this.points[0].x
                    && x <= _this.points[3].x
                    && y >= _this.points[0].y
                    && y <= _this.points[3].y) {
                    return true;
                }
                else {
                    return false;
                }
            };
            this.data = data;
            this.points.push({ x: x, y: y });
            this.points.push({ x: x + w, y: y });
            this.points.push({ x: x + w, y: y + w });
            this.points.push({ x: x, y: y + w });
            this.center = { x: x + w / 2, y: y + h / 2 };
        }
        return RectangleZone;
    }(EmptyZone));
    Zonerism.RectangleZone = RectangleZone;
    /**
    Radial zoning
    */
    var RadialZone = (function (_super) {
        __extends(RadialZone, _super);
        function RadialZone(cx, cy, ang1, ang2, r1, r2, data) {
            if (data === void 0) { data = {}; }
            _super.call(this);
            this.cx = cx;
            this.cy = cy;
            this.ang1 = ang1;
            this.ang2 = ang2;
            this.r1 = r1;
            this.r2 = r2;
            this.data = data;
            this.points = [];
            this.sectorSize = 20;
            if (ang1 > ang2) {
                ang1 -= 2 * Math.PI;
            }
            var theta = ang2 - ang1;
            var nsteps = Math.floor((theta * r2) / this.sectorSize);
            var step = theta / nsteps;
            for (var i = 0; i <= nsteps; i++) {
                var a = ang1 + i * step;
                var pointx = cx + r2 * Math.sin(a);
                var pointy = cy - r2 * Math.cos(a);
                var point = { x: pointx, y: pointy };
                this.points.push(point);
            }
            //there is no middle sometimes
            if (Math.abs(theta - 2 * Math.PI) > 0.1) {
                if (r1 == 0) {
                    this.points.push({ x: cx, y: cy });
                }
                else {
                    nsteps = Math.floor((theta * r1) / this.sectorSize);
                    step = theta / nsteps;
                    for (var i = 0; i <= nsteps; i++) {
                        var a = ang2 - i * step;
                        var pointx = cx + r1 * Math.sin(a);
                        var pointy = cy - r1 * Math.cos(a);
                        var point = { x: pointx, y: pointy };
                        console.log("Point added");
                        this.points.push(point);
                    }
                }
            }
            if (Math.abs((ang2 - ang1) - 2 * Math.PI) < 1 / 32) {
                this.center = { x: cx, y: cy };
            }
            else {
                var midang = (ang1 + ang2) / 2;
                var pointx = cx + ((r1 + r2) / 2) * Math.sin(midang);
                var pointy = cy - ((r1 + r2) / 2) * Math.cos(midang);
                var point = { x: pointx, y: pointy };
                this.center = point;
            }
        }
        return RadialZone;
    }(EmptyZone));
    Zonerism.RadialZone = RadialZone;
    /**
    A class that decorates a zone with the active zone interface allowing switchable callback
    */
    var ZoneActivator = (function (_super) {
        __extends(ZoneActivator, _super);
        function ZoneActivator(za, f) {
            _super.call(this);
            this.act = function () { };
            this.zoneAct = function () { };
            this.setCallBack = function (cb) {
                var _this = this;
                this.act = function () { cb(_this.internal); console.log("this zone data:" + _this.data); };
            };
            this.internal = za;
            this.contains = za.contains;
            this.data = za.data;
            this.points = za.points;
            this.act = function () { f(za); };
            this.center = za.center;
        }
        return ZoneActivator;
    }(EmptyZone));
    Zonerism.ZoneActivator = ZoneActivator;
})(Zonerism || (Zonerism = {}));
var entities;
(function (entities) {
    var Paddle = (function (_super) {
        __extends(Paddle, _super);
        function Paddle(x, y, radius, arcRange) {
            _super.call(this, gameRoot.game, x, y);
            this.active = true;
            this.radius = radius;
            this._arcRange = arcRange;
            this.ang1 = this.rotation - this._arcRange / 2;
            this.ang2 = this.rotation + this._arcRange / 2;
            this.thickness = 33;
            this.anchor.setTo(0.5);
            this.bmdRing = this.game.add.graphics(0, 0);
            this.bmdRing.lineStyle(14, 0xE6E6E6);
            this.bmdRing.drawCircle(this.x, this.y, 2 * this.radius - this.thickness);
            this.bmd = this.game.add.bitmapData(2 * this.radius, 2 * this.radius);
            this.loadTexture(this.bmd);
            this.updateZone();
            this.game.add.existing(this);
        }
        Object.defineProperty(Paddle.prototype, "arcRange", {
            get: function () {
                return this._arcRange;
            },
            set: function (range) {
                this._arcRange = range;
                this.updateZone();
            },
            enumerable: true,
            configurable: true
        });
        Paddle.prototype.changeSize = function (newRange, duration) {
            // Tween size change ...
            this.game.add.tween(this).to({ arcRange: newRange }, duration, Phaser.Easing.Quadratic.Out, true);
        };
        Paddle.prototype.updateZone = function () {
            // Called every time a visual change is required ...
            this.ang1 = (this.rotation - this._arcRange / 2) + Math.PI / 2;
            this.ang2 = (this.rotation + this._arcRange / 2) + Math.PI / 2;
            var zone = new Zonerism.RadialZone(this.radius, this.radius, this.ang1, this.ang2, this.radius - this.thickness, this.radius);
            this.drawPaddle(zone);
        };
        Paddle.prototype.drawPaddle = function (zone) {
            this.bmd.clear();
            var p1 = zone.points[0];
            this.bmd.ctx.beginPath();
            this.bmd.ctx.moveTo(p1.x, p1.y);
            for (var j = 1; j < zone.points.length; j++) {
                p1 = zone.points[j];
                this.bmd.ctx.lineTo(p1.x, p1.y);
                console.log("Moved line to x:" + p1.x + " y:" + p1.y);
            }
            this.bmd.ctx.fillStyle = '#65C8D0';
            this.bmd.ctx.fill();
        };
        return Paddle;
    }(Phaser.Sprite));
    entities.Paddle = Paddle;
})(entities || (entities = {}));
var entities;
(function (entities) {
    var Ball = (function (_super) {
        __extends(Ball, _super);
        function Ball(x, y, radius, baseSpeed, paddle) {
            _super.call(this, gameRoot.game, x, y);
            this.speedFactor = 100;
            this.maximumSpeed = 300;
            this.radius = radius;
            this.speed = baseSpeed;
            this.baseSpeed = baseSpeed;
            this.speedFactor = baseSpeed;
            this.anchor.setTo(0.5);
            this.rotation = Math.random() * (2 * Math.PI);
            this.paddle = paddle;
            this.active = false;
            this.bmd = this.game.add.bitmapData(2 * radius, 2 * radius);
            this.bmd.circle(radius, radius, radius, '#507C99');
            this.loadTexture(this.bmd);
            this.game.physics.enable(this, Phaser.Physics.ARCADE);
            this.body.enable = true;
            this.checkWorldBounds = true;
            this.events.onOutOfBounds.add(this.gameReset, this);
            this.game.add.existing(this);
        }
        Ball.prototype.update = function () {
            var dx = this.x - this.paddle.x;
            var dy = this.y - this.paddle.y;
            var distance = Math.sqrt(dx * dx + dy * dy);
            var paddleInner = this.paddle.radius - this.paddle.thickness - this.radius;
            var paddleOuter = this.paddle.radius + this.paddle.thickness - this.radius;
            if (distance > paddleInner && distance < paddleOuter) {
                //You are at the outer edge of the paddle ...
                var paddleRot = this.paddle.rotation;
                var paddleArc = this.paddle.arcRange;
                var ballAngle = Math.atan2(this.y - this.paddle.y, this.x - this.paddle.x);
                if (Utils.inCircleBound(ballAngle, Utils.circleWrap(paddleRot - paddleArc / 2), Utils.circleWrap(paddleRot + paddleArc / 2))) {
                    //Ball has hit the actual paddle ...
                    this.game.sound.play("note" + this.game.rnd.integerInRange(0, 6), 1, false);
                    var maxKickupAngle = Math.PI / 2;
                    var maxPaddleAngle = Math.PI / 4;
                    var maxSliceVelocity = Math.PI * 0.01;
                    var maxSliceAngle = Math.PI / 4;
                    var reflected = Math.PI + 2 * ballAngle - this.rotation;
                    var kickup = (2 * maxKickupAngle * Utils.shortAngle(ballAngle, this.rotation) / Math.PI);
                    var convexity = (2 * maxPaddleAngle / paddleArc * Utils.shortAngle(ballAngle, this.paddle.rotation));
                    var sliceV = gameRoot.gameScreen.paddleController.thetaV;
                    var sliceAng = maxSliceAngle * Math.min(Math.abs(sliceV), maxSliceVelocity) / maxSliceVelocity * (Math.abs(sliceV) / sliceV);
                    var clippedReflection = reflected + convexity + kickup + sliceAng;
                    //Bring the ball back to the paddleInner ...
                    this.rotation = clippedReflection;
                    var spacer = 5;
                    this.x = this.paddle.x + (paddleInner - spacer) * Math.cos(ballAngle);
                    this.y = this.paddle.y + (paddleInner - spacer) * Math.sin(ballAngle);
                    gameRoot.gameScreen.score++;
                    if (gameRoot.gameScreen.score > gameRoot.gameScreen.highscore) {
                        gameRoot.gameScreen.highscore = gameRoot.gameScreen.score;
                    }
                    this.increaseSpeed();
                }
            }
            if (this.active) {
                this.game.physics.arcade.velocityFromRotation(this.rotation, this.speed, this.body.velocity);
            }
        };
        Ball.prototype.increaseSpeed = function () {
            //logarithmic
            this.speed = this.baseSpeed + this.speedFactor * Math.log(1 + gameRoot.gameScreen.score);
        };
        Ball.prototype.gameReset = function () {
            this.x = gameRoot.game.width / 2;
            this.y = gameRoot.game.height / 2;
            this.body.velocity.x = 0;
            this.body.velocity.y = 0;
            this.active = false;
            this.speed = this.baseSpeed;
            this.rotation = Math.random() * Math.PI * 2;
            gameRoot.gameScreen.score = 0;
            gameRoot.gameScreen.scoreText.setText("");
        };
        return Ball;
    }(Phaser.Sprite));
    entities.Ball = Ball;
})(entities || (entities = {}));
var gameUI;
(function (gameUI) {
    var RingButton = (function (_super) {
        __extends(RingButton, _super);
        function RingButton(game, x, y, key, callback, context) {
            _super.call(this, gameRoot.game, x, y);
            this.buttonRadius = 90;
            this.callback = callback.bind(context);
            var strokeThickness = 3;
            this.buttonRing = new Phaser.BitmapData(game, 'ring', 2 * this.buttonRadius + 2 * strokeThickness, 2 * this.buttonRadius + 2 * strokeThickness);
            this.buttonRing.ctx.strokeStyle = '#E6E6E6';
            this.buttonRing.ctx.fillStyle = '#FFFFFF';
            this.buttonRing.ctx.lineWidth = 2;
            this.buttonRing.circle(this.buttonRadius + strokeThickness, this.buttonRadius + strokeThickness, this.buttonRadius);
            this.buttonRing.ctx.stroke();
            this.anchor.setTo(0.5);
            this.loadTexture(this.buttonRing);
            this.inputEnabled = true;
            this.events.onInputDown.add(this.onDown, this);
            this.events.onInputUp.add(this.onUp, this);
            gameRoot.game.add.existing(this);
            this.buttonImage = gameRoot.game.add.image(this.x, this.y, key);
            this.buttonImage.anchor.setTo(0.5);
            this.buttonImage.scale.setTo(0.6);
        }
        RingButton.prototype.onDown = function () {
            this.buttonRing.ctx.fillStyle = '#f2f2f2';
            this.buttonRing.ctx.fill();
        };
        RingButton.prototype.onUp = function () {
            this.buttonRing.ctx.fillStyle = '#FFFFFF';
            this.buttonRing.ctx.fill();
            var pointer = gameRoot.game.input.activePointer;
            if (Phaser.Math.distance(this.x, this.y, pointer.x, pointer.y) >= this.buttonRadius) {
                //Valid button press ...
                this.callback();
            }
        };
        return RingButton;
    }(Phaser.Sprite));
    gameUI.RingButton = RingButton;
})(gameUI || (gameUI = {}));
var AR169 = {};
var AR1610 = {};
var AR43 = {};
document.addEventListener("deviceready", onDeviceReady, false);
var _manifest;
var gameRoot;
var ARData;
function onDeviceReady() {
    console.log("onDeviceReady()");
    // Listen for the device's backbutton, nd route it to our callback
    document.addEventListener("backbutton", onExit.bind(this), false);
    // Initialise the Tgoma library. 'content' is the name of the div the ActionBar will attach itself to.
    Tgoma.init('content', onTgomaInitialised);
}
function onTgomaInitialised(p_parentManifest) {
    console.log("onTgomaInitialised()");
    //Game ID
    TgomaMessaging.Game.setGameId("stripped");
    // Load our manifest so we can begin to get file paths from it.
    _manifest = new Tgoma.ManifestLoader.Manifest("manifest.xml", onManifestLoadComplete, null);
    startSession();
    // Layout logic
    initScreen();
    postInitScreen();
}
function initScreen() {
    gameRoot = new PongGame();
    gameRoot.setupStates();
}
function postInitScreen() {
    // Calls with callbacks referring to the game
    //When the user changes the game needs to reset to be in the right state
    // The ActionBar has been initialised now - we should listen for the user trying to quit
    ActionBar.onBackClickedSignal.add(onExit, this);
    ActionBar.onShowActionBarSignal.add(gameRoot.pause, gameRoot);
    ActionBar.onHideActionBarSignal.add(gameRoot.resume, gameRoot);
}
function onManifestLoadComplete() {
    console.log("onManifestLoadComplete()");
}
function startSession() {
    // Create a session - we're playing level 0
    TgomaMessaging.Session.beginSession(0);
}
// The user has chosen to exit!
function onExit(event) {
    gameRoot.end();
    // End any sessions we may have had going
    TgomaMessaging.Session.endSession(0);
    // Return to the parent application
    TgomaMessaging.Game.returnToMainApplication();
}
var PongGame = (function () {
    function PongGame() {
        this.states = {};
        var aspectRatio = window.innerWidth / window.innerHeight;
        if (aspectRatio > 1.7) {
            ARData = AR169;
            console.log("Aspect ratio = 16:9");
            this.game = new Phaser.Game(1280, 720, Phaser.CANVAS, 'content');
        }
        else if (aspectRatio > 1.45) {
            ARData = AR1610;
            console.log("Aspect ratio = 16:10");
            this.game = new Phaser.Game(1280, 800, Phaser.CANVAS, 'content');
        }
        else {
            ARData = AR43;
            console.log("Aspect ratio = 4:3");
            this.game = new Phaser.Game(1024, 768, Phaser.CANVAS, 'content');
        }
        this.gw = this.game.width;
        this.gh = this.game.height;
    }
    PongGame.prototype.setupStates = function () {
        this.loadingScreen = this.states["loading"] = new screens.LoadingScreen();
        this.gameScreen = this.states["game"] = new screens.GameScreen();
        //Add states to game object
        for (var state in this.states) {
            this.game.state.add(state, this.states[state], false);
            console.log(state + " state added to game object");
        }
        //Start Loading screen
        this.game.state.start("loading", false, false);
    };
    PongGame.prototype.pause = function () {
    };
    PongGame.prototype.resume = function () {
    };
    PongGame.prototype.reset = function () {
    };
    PongGame.prototype.end = function () {
        // End any sessions we may have had going
        TgomaMessaging.Session.endSession(0);
        // Return to the parent application
        TgomaMessaging.Game.returnToMainApplication();
    };
    return PongGame;
}());
/// <reference path="../tsdefs/phaser.d.ts"/>
/// <reference path="./screens/gameScreen.ts"/>
/// <reference path="./screens/loadingScreen.ts"/>
/// <reference path="../sfgametools/Utils.ts"/>
/// <reference path="../sfgametools/control/game-pads.ts"/>
/// <reference path="../sfgametools/zonelib/zone.ts"/>
/// <reference path="./entities/paddle.ts"/>
/// <reference path="./entities/ball.ts"/>
/// <reference path="./entities/ui/bmdButton.ts"/>
/// <reference path="./data/ARDATA.ts"/>
/// <reference path="../src/boilerplate.ts"/>
/// <reference path="../src/game.ts"/>
