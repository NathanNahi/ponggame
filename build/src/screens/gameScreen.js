var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var screens;
(function (screens) {
    var GameScreen = (function (_super) {
        __extends(GameScreen, _super);
        function GameScreen() {
            _super.call(this);
        }
        GameScreen.prototype.create = function () {
            this.game.sound.play("loop", 0.7, true);
            gameRoot.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.paddleController = new control.RadialFollower(0.1, 0, true);
            this.paddleController.applyVFollower(0.01);
            this.paddleController.onPolarLocation.add(this.startGame, this);
            var scoreStyle = { font: 'myriadpro', fontSize: '300px', fill: '#79D1CB' };
            this.score = 0;
            this.scoreText = gameRoot.game.add.text(gameRoot.game.width / 2, gameRoot.game.height / 2, "", scoreStyle);
            this.scoreText.anchor.setTo(0.5);
            var highscoreStyle = { font: 'myriadpro', fontSize: '130px', fill: '#79D1CB' };
            this.highscore = 0;
            this.highscoreText = gameRoot.game.add.text(gameRoot.game.width / 10 * 9, gameRoot.game.height / 10 * 1, "", highscoreStyle);
            this.highscoreText.setText(this.highscore.toString());
            this.highscoreText.anchor.setTo(0.5);
            this.paddle = new entities.Paddle(gameRoot.game.width / 2, gameRoot.game.height / 2, gameRoot.game.height / 2 - 50, (2 * Math.PI) / 4);
            this.ball = new entities.Ball(gameRoot.game.width / 2, gameRoot.game.height / 2, 30, 70, this.paddle);
            this.shadeLayer = new entities.ShadingLayer(this.game, this.ball, 0.01);
            this.game.world.sendToBack(this.shadeLayer);
            this.home = new gameUI.RingButton(gameRoot.game, gameRoot.game.width / 2, gameRoot.game.height / 2, 'icon_home', function () { console.log("Bloop"); }, this);
        };
        GameScreen.prototype.update = function () {
            this.paddleController.update(gameRoot.game.time.elapsed);
            this.paddle.rotation = -this.paddleController.theta;
            this.paddle.rotation = Utils.circleWrap(this.paddle.rotation);
            this.ball.rotation = Utils.circleWrap(this.ball.rotation);
            this.highscoreText.setText(this.highscore.toString());
            if (this.score > 0) {
                this.scoreText.setText(this.score.toString());
            }
        };
        GameScreen.prototype.render = function () {
            this.shadeLayer.render();
        };
        GameScreen.prototype.startGame = function () {
            if (!this.ball.active) {
                this.ball.active = true;
            }
        };
        return GameScreen;
    }(Phaser.State));
    screens.GameScreen = GameScreen;
})(screens || (screens = {}));
