var ascanner = require("../sfgametools/assetTools/assetScanner.js")
var fs = require("fs")

// Compile the Atlas
var assetPack = ascanner("./assets", null , null, 1)
fs.writeFileSync("./assets/assets.json", JSON.stringify(assetPack, null,"  "))

// compile type key Json
var assetKeys = ascanner("./assets", undefined, undefined, Infinity)
fs.writeFileSync("./assets/json/assetKeys.json", JSON.stringify(assetKeys, null,"  "))
