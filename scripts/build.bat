git describe --dirty > ver.tmp
set /p ver= <ver.tmp
del ver.tmp
echo {^"version^":^" %ver% ^"} > ./assets/json/buildinfo.json

tsc -t ES5 --outFile app.js src/refall.ts
