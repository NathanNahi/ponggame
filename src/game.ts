class PongGame {

    game:Phaser.Game;
    states = {};

    loadingScreen:screens.LoadingScreen;
    gameScreen:screens.GameScreen;

    gw:number;
    gh:number;

    constructor(){

        var aspectRatio: number = window.innerWidth / window.innerHeight;

        if (aspectRatio > 1.7) //Use 16:9
        {
            ARData = AR169;
            console.log("Aspect ratio = 16:9");
            this.game = new Phaser.Game(1280, 720, Phaser.CANVAS, 'content');
        }
        else if (aspectRatio > 1.45)//Use 16:10
        {
            ARData = AR1610;
            console.log("Aspect ratio = 16:10");
            this.game = new Phaser.Game(1280, 800, Phaser.CANVAS, 'content');
        }
        else //Use 4:3
        {
            ARData = AR43
            console.log("Aspect ratio = 4:3");
            this.game = new Phaser.Game(1024, 768, Phaser.CANVAS, 'content');
        }

        this.gw = this.game.width;
        this.gh = this.game.height;
    }

    setupStates() {
        this.loadingScreen = this.states["loading"] = new screens.LoadingScreen();
        this.gameScreen = this.states["game"] = new screens.GameScreen();

        //Add states to game object
        for (var state in this.states) {
            this.game.state.add(state, this.states[state], false);
            console.log(state + " state added to game object")
        }

        //Start Loading screen
        this.game.state.start("loading", false, false);
    }

    pause(){

    }

    resume(){

    }

    reset(){

    }

    end(){
        // End any sessions we may have had going
        TgomaMessaging.Session.endSession(0);

        // Return to the parent application
        TgomaMessaging.Game.returnToMainApplication();
    }

}
