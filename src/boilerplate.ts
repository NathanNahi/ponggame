document.addEventListener("deviceready", onDeviceReady, false);

var _manifest;
var gameRoot:PongGame;
var ARData:ARDATA;

function onDeviceReady() {
    console.log("onDeviceReady()");

    // Listen for the device's backbutton, nd route it to our callback
    document.addEventListener("backbutton", onExit.bind(this), false);

    // Initialise the Tgoma library. 'content' is the name of the div the ActionBar will attach itself to.
    Tgoma.init('content', onTgomaInitialised);
}

function onTgomaInitialised(p_parentManifest) {
    console.log("onTgomaInitialised()");

    //Game ID
    TgomaMessaging.Game.setGameId("stripped");

	// Load our manifest so we can begin to get file paths from it.
    _manifest = new Tgoma.ManifestLoader.Manifest("manifest.xml", onManifestLoadComplete, null);



    startSession();

    // Layout logic
    initScreen();
    postInitScreen();

}

function initScreen() {
    gameRoot = new PongGame();
    gameRoot.setupStates();
}

function postInitScreen(){
    // Calls with callbacks referring to the game

        //When the user changes the game needs to reset to be in the right state

    // The ActionBar has been initialised now - we should listen for the user trying to quit
    ActionBar.onBackClickedSignal.add(onExit, this);
    ActionBar.onShowActionBarSignal.add(gameRoot.pause, gameRoot);
    ActionBar.onHideActionBarSignal.add(gameRoot.resume, gameRoot);

}

function onManifestLoadComplete() {
    console.log("onManifestLoadComplete()");

}

function startSession() {

	// Create a session - we're playing level 0
    TgomaMessaging.Session.beginSession(0);
}


// The user has chosen to exit!
function onExit(event) {
    gameRoot.end();

    // End any sessions we may have had going
    TgomaMessaging.Session.endSession(0);

    // Return to the parent application
    TgomaMessaging.Game.returnToMainApplication();
}
