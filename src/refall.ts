/// <reference path="../tsdefs/phaser.d.ts"/>

/// <reference path="./screens/gameScreen.ts"/>
/// <reference path="./screens/loadingScreen.ts"/>

/// <reference path="../sfgametools/Utils.ts"/>
/// <reference path="../sfgametools/control/game-pads.ts"/>
/// <reference path="../sfgametools/zonelib/zone.ts"/>

/// <reference path="./entities/paddle.ts"/>
/// <reference path="./entities/ball.ts"/>
/// <reference path="./entities/shadeLayer.ts"/>
/// <reference path="./entities/ui/bmdButton.ts"/>

/// <reference path="./data/ARDATA.ts"/>

/// <reference path="../src/boilerplate.ts"/>
/// <reference path="../src/game.ts"/>
