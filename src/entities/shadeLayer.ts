

//draw objects onto the shade bmd and also render a semitransparent block onto a bitmap
module entities {
    export class ShadingLayer extends Phaser.Sprite{

        bmd:Phaser.BitmapData;

        constructor(public game:Phaser.Game, private ball:Ball, private decayRate:number){
            super(game, 0,0)
            this.game.add.existing(this)
            this.bmd = new Phaser.BitmapData(this.game, "shade", this.game.width, this.game.height);
            var ctx = this.bmd.canvas.getContext("2d");
            ctx.fillStyle = "#F0F0F0";
            ctx.fillRect(0,0,this.game.width, this.game.height);

            this.loadTexture(this.bmd)



        }

        render(){
            var ctx = this.bmd.canvas.getContext("2d");

            ctx.drawImage(this.ball.texture.baseTexture.source, 0, 0, this.ball.width, this.ball.height, this.ball.x-this.ball.width/2, this.ball.y-this.ball.height/2, this.ball.width, this.ball.height);
            //ctx.globalAlpha = this.decayRate
            ctx.fillStyle = "rgba(240,240,240,0.1)";
            ctx.fillRect(0,0,this.game.width, this.game.height);


        }



    }
}
