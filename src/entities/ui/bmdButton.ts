module gameUI {
    export class RingButton extends Phaser.Sprite {

        buttonRing:Phaser.BitmapData;
        buttonImage:Phaser.Image;
        buttonRadius:number = 90;

        callback:()=>{};
        buttonBounds:Phaser.Rectangle;

        constructor(game:Phaser.Game, x:number, y:number, key:string, callback:()=>void, context:any) {
            super(gameRoot.game, x, y);

            this.callback = callback.bind(context);

            var strokeThickness = 3;

            this.buttonRing = new Phaser.BitmapData(game, 'ring', 2*this.buttonRadius + 2*strokeThickness, 2*this.buttonRadius + 2*strokeThickness);
            this.buttonRing.ctx.strokeStyle = '#E6E6E6';
            this.buttonRing.ctx.fillStyle = '#FFFFFF';
            this.buttonRing.ctx.lineWidth = 2;
            this.buttonRing.circle(this.buttonRadius + strokeThickness, this.buttonRadius + strokeThickness, this.buttonRadius);
            this.buttonRing.ctx.stroke();
            this.anchor.setTo(0.5);

            this.loadTexture(this.buttonRing);

            this.inputEnabled = true;
            this.events.onInputDown.add(this.onDown, this);
            this.events.onInputUp.add(this.onUp, this);

            gameRoot.game.add.existing(this);

            this.buttonImage = gameRoot.game.add.image(this.x, this.y, key);
            this.buttonImage.anchor.setTo(0.5);
            this.buttonImage.scale.setTo(0.6);
        }

        onDown() {
            this.buttonRing.ctx.fillStyle = '#f2f2f2';
            this.buttonRing.ctx.fill();
        }

        onUp() {
            this.buttonRing.ctx.fillStyle = '#FFFFFF';
            this.buttonRing.ctx.fill();

            var pointer = gameRoot.game.input.activePointer;
            if (Phaser.Math.distance(this.x, this.y, pointer.x, pointer.y) >= this.buttonRadius) {
                //Valid button press ...
                this.callback();
            }
        }
    }
}
