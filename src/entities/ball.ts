module entities {
    export class Ball extends Phaser.Sprite {
        baseSpeed:number;
        speedFactor = 100;
        maximumSpeed = 300;

        speed:number;
        radius:number;

        collided:boolean;

        active:boolean;

        paddle:entities.Paddle;

        bmd:Phaser.BitmapData;

        constructor(x:number, y:number, radius:number, baseSpeed:number, paddle:entities.Paddle) {
            super(gameRoot.game, x, y);

            this.radius = radius;
            this.speed = baseSpeed;
            this.baseSpeed = baseSpeed;
            this.speedFactor = baseSpeed;
            this.anchor.setTo(0.5);
            this.rotation = Math.random() * (2*Math.PI);

            this.paddle = paddle;

            this.active = false;

            this.bmd = this.game.add.bitmapData(2*radius, 2*radius);
            this.bmd.circle(radius, radius, radius, '#507C99');

            this.loadTexture(this.bmd);

            this.game.physics.enable(this, Phaser.Physics.ARCADE);
            this.body.enable = true;

            this.checkWorldBounds = true;
            this.events.onOutOfBounds.add(this.gameReset, this);

            this.game.add.existing(this);
        }

        update() {

            var dx = this.x - this.paddle.x;
            var dy = this.y - this.paddle.y;
            var distance = Math.sqrt(dx * dx + dy * dy);
            var paddleInner = this.paddle.radius - this.paddle.thickness - this.radius;
            var paddleOuter = this.paddle.radius + this.paddle.thickness - this.radius;

            if (distance > paddleInner && distance < paddleOuter) {
                //You are at the outer edge of the paddle ...
                var paddleRot = this.paddle.rotation;
                var paddleArc = this.paddle.arcRange;
                var ballAngle = Math.atan2(this.y - this.paddle.y, this.x - this.paddle.x);

                if (Utils.inCircleBound(ballAngle, Utils.circleWrap(paddleRot - paddleArc / 2), Utils.circleWrap(paddleRot + paddleArc / 2))) {
                    //Ball has hit the actual paddle ...
                    this.game.sound.play("note"+this.game.rnd.integerInRange(0,6), 1, false)

                    var maxKickupAngle = Math.PI / 2;
                    var maxPaddleAngle = Math.PI / 4;
                    var maxSliceVelocity = Math.PI*0.01;
                    var maxSliceAngle = Math.PI /4;

                    var reflected = Math.PI + 2*ballAngle - this.rotation;
                    var kickup = (2 * maxKickupAngle * Utils.shortAngle(ballAngle, this.rotation) / Math.PI)
                    var convexity = (2 * maxPaddleAngle / paddleArc * Utils.shortAngle(ballAngle, this.paddle.rotation))

                    var sliceV = gameRoot.gameScreen.paddleController.thetaV
                    var sliceAng = maxSliceAngle * Math.min(Math.abs(sliceV), maxSliceVelocity)/maxSliceVelocity * (Math.abs(sliceV)/sliceV)

                    var clippedReflection = reflected + convexity + kickup + sliceAng;

                    //Bring the ball back to the paddleInner ...
                    this.rotation = clippedReflection;

                    var spacer = 5;
                    this.x = this.paddle.x + (paddleInner-spacer) * Math.cos(ballAngle);
                    this.y = this.paddle.y + (paddleInner-spacer) * Math.sin(ballAngle);

                    gameRoot.gameScreen.score ++;
                    if (gameRoot.gameScreen.score > gameRoot.gameScreen.highscore) {
                        gameRoot.gameScreen.highscore = gameRoot.gameScreen.score;
                    }

                    this.increaseSpeed();
                    //Very broken ...
                    //this.paddle.changeSize(this.paddle.arcRange + Math.PI/8, 300);
                }
            }

            if (this.active) {
                this.game.physics.arcade.velocityFromRotation(this.rotation, this.speed, this.body.velocity);
            }
        }

        increaseSpeed(){

            //logarithmic
            this.speed = this.baseSpeed + this.speedFactor*Math.log(1 + gameRoot.gameScreen.score)

        }

        gameReset() {
            this.x = gameRoot.game.width / 2;
            this.y = gameRoot.game.height / 2;

            this.body.velocity.x = 0;
            this.body.velocity.y = 0;

            this.active = false;
            this.speed = this.baseSpeed;
            this.rotation = Math.random() * Math.PI*2;

            gameRoot.gameScreen.score = 0;
            gameRoot.gameScreen.scoreText.setText("");
        }
    }
}
