module entities {
    export class Paddle extends Phaser.Sprite{

        bmd:Phaser.BitmapData;
        bmdRing:Phaser.Graphics;

        private _arcRange:number;

        get arcRange():number{
            return this._arcRange;
        }
        set arcRange(range:number){
            this._arcRange = range;
            this.updateZone();
        }

        ang1:number;
        ang2:number;
        thickness:number;
        radius:number;

        active:boolean;

        constructor(x:number, y:number, radius:number, arcRange:number) {
            super(gameRoot.game, x, y);

            this.active = true;

            this.radius = radius;
            this._arcRange = arcRange;
            this.ang1 = this.rotation - this._arcRange / 2;
            this.ang2 = this.rotation + this._arcRange / 2;
            this.thickness = 33;

            this.anchor.setTo(0.5);

            this.bmdRing = this.game.add.graphics(0, 0);
            this.bmdRing.lineStyle(14, 0xE6E6E6);
            this.bmdRing.drawCircle(this.x, this.y, 2*this.radius - this.thickness);

            this.bmd = this.game.add.bitmapData(2*this.radius, 2*this.radius);
            this.loadTexture(this.bmd);

            this.updateZone();

            this.game.add.existing(this);
        }

        changeSize(newRange:number, duration:number) {
            // Tween size change ...
            this.game.add.tween(this).to({arcRange:newRange}, duration, Phaser.Easing.Quadratic.Out, true);
        }

        updateZone() {
            // Called every time a visual change is required ...
            this.ang1 = (this.rotation - this._arcRange / 2) + Math.PI / 2;
            this.ang2 = (this.rotation + this._arcRange / 2) + Math.PI / 2;
            var zone = new Zonerism.RadialZone(this.radius, this.radius, this.ang1, this.ang2, this.radius - this.thickness, this.radius);
            this.drawPaddle(zone);
        }

        drawPaddle(zone:Zonerism.Zone){

            this.bmd.clear();

            var p1 = zone.points[0];

            this.bmd.ctx.beginPath();
            this.bmd.ctx.moveTo(p1.x, p1.y);

            for (var j = 1; j < zone.points.length; j++){
                p1 = zone.points[j];
                this.bmd.ctx.lineTo(p1.x,p1.y);
                console.log(`Moved line to x:${p1.x} y:${p1.y}`);
            }

            this.bmd.ctx.fillStyle = '#65C8D0';
            this.bmd.ctx.fill();
        }
    }
}
