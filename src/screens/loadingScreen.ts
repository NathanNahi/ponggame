module screens {
    export class LoadingScreen extends Phaser.State {

        constructor() {
            super();
        }

        create() {
            console.log("Loading Screen reached");
            gameRoot.game.load.onLoadStart.add(this.onLoadStart, this);
            gameRoot.game.load.onLoadComplete.add(this.onLoadComplete, this);

            this.prepareLoadStack();
        }

        prepareLoadStack() {
            // Add the things ...
            gameRoot.game.load.image("icon_leaderboard", "assets/icon-leaderboard.png");
            gameRoot.game.load.image("icon_home", "assets/icon-home.png");
            gameRoot.game.load.image("button_play", "assets/button-play.png");

            gameRoot.game.load.audio("loop", "assets/sound/pongLoop.mp3", true)
            gameRoot.game.load.audio("note0", "assets/sound/pongC.mp3", true)
            gameRoot.game.load.audio("note1", "assets/sound/pongD.mp3", true)
            gameRoot.game.load.audio("note2", "assets/sound/pongE.mp3", true)
            gameRoot.game.load.audio("note3", "assets/sound/pongF.mp3", true)
            gameRoot.game.load.audio("note4", "assets/sound/pongG.mp3", true)
            gameRoot.game.load.audio("note5", "assets/sound/pongA.mp3", true)
            gameRoot.game.load.audio("note6", "assets/sound/pongB.mp3", true)

            gameRoot.game.load.start();
        }

        onLoadStart() {
            console.log("Loading started ...")
        }

        onLoadComplete() {
            console.log("Loading completed ...")



            gameRoot.game.state.start("game");
        }
    }
}
