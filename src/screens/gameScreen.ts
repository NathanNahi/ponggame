module screens {
    export class GameScreen extends Phaser.State {

        paddleController:control.RadialFollower;
        paddle:entities.Paddle;
        ball:entities.Ball;
        bmdRing:Phaser.Graphics;
        shadeLayer:entities.ShadingLayer;


        score:number;
        scoreText:Phaser.Text;

        highscore:number;
        highscoreText:Phaser.Text;

        home:gameUI.RingButton;

        constructor() {
            super();
        }

        create() {
            this.game.sound.play("loop", 0.7, true)

            gameRoot.game.physics.startSystem(Phaser.Physics.ARCADE);
            //gameRoot.game.stage.backgroundColor = "#F0F0F0";

            this.paddleController = new control.RadialFollower(0.1, 0, true);
            this.paddleController.applyVFollower(0.01);
            this.paddleController.onPolarLocation.add(this.startGame, this);

            var scoreStyle  = { font: 'myriadpro', fontSize: '300px', fill: '#79D1CB' };
            this.score = 0;
            this.scoreText = gameRoot.game.add.text(gameRoot.game.width / 2, gameRoot.game.height / 2, "", scoreStyle);
            this.scoreText.anchor.setTo(0.5);

            var highscoreStyle  = { font: 'myriadpro', fontSize: '130px', fill: '#79D1CB' };
            this.highscore = 0;
            this.highscoreText = gameRoot.game.add.text(gameRoot.game.width / 10 * 9, gameRoot.game.height / 10 * 1, "", highscoreStyle);
            this.highscoreText.setText(this.highscore.toString());
            this.highscoreText.anchor.setTo(0.5);

            this.paddle = new entities.Paddle(gameRoot.game.width / 2, gameRoot.game.height / 2, gameRoot.game.height / 2 - 50, (2*Math.PI)/4);
            this.ball = new entities.Ball(gameRoot.game.width / 2, gameRoot.game.height / 2, 30, 70, this.paddle);

            this.shadeLayer = new entities.ShadingLayer(this.game, this.ball, 0.01)
            this.game.world.sendToBack(this.shadeLayer)

            this.home = new gameUI.RingButton(gameRoot.game, gameRoot.game.width / 2, gameRoot.game.height / 2, 'icon_home', ()=>{console.log("Bloop")}, this);
        }

        update() {
            this.paddleController.update(gameRoot.game.time.elapsed);
            this.paddle.rotation = -this.paddleController.theta;
            this.paddle.rotation = Utils.circleWrap(this.paddle.rotation);
            this.ball.rotation = Utils.circleWrap(this.ball.rotation);

            this.highscoreText.setText(this.highscore.toString());
            if (this.score > 0) {
                this.scoreText.setText(this.score.toString());
            }
        }

        render(){
            this.shadeLayer.render()
        }

        startGame() {
            if(!this.ball.active) {
                this.ball.active = true;
            }
        }
    }
}
