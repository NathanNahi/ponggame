var Tgoma;
(function (Tgoma) {
    /**
     * Initialises all of the Tgoma libraries, and sets up the actionbar. It will then provide a callback when everything is good to use.
     * This requires cordova to be ready, so should be called from the onDeviceReady document event.
     * @param p_mainElementId The name of the DOM element that your game is attached to. The ActionBar will attach itself to the same element
     * @param p_callback A callback to be used when all of the Tgoma libraries have been loaded and are ready to use. It contains a Tgoma.ManifestLoader.Manifest object with the parent app's manifest, used to get some common assets such as flags.
     */
    function init(p_mainElementId, p_callback, p_useActionbar) {
        var _this = this;
        if (p_useActionbar === void 0) { p_useActionbar = true; }
        //If running on a computer we don't need to initialise most things.
        if (!Tgoma.ApplicationUtils.DeviceUtils.isMobile.any()) {
            Tgoma.BatteryHelper.init();
            Tgoma.HighScoreHelper.init();
            Tgoma.TrampolineHelper.init();
            Tgoma.EventHelper.init();
            Tgoma.ErrorHelper.init();
            if (p_callback != null) {
                p_callback(null);
            }
            return;
        }
        Tgoma.BatteryHelper.init();
        Tgoma.ConnectionHelper.init();
        Tgoma.HighScoreHelper.init();
        Tgoma.TrampolineHelper.init();
        Tgoma.UserHelper.init();
        Tgoma.EventHelper.init();
        Tgoma.ErrorHelper.init();
        screen.lockOrientation('landscape');
        TgomaMessaging.Internal.ApplicationManagement.getGoogleAnalyticsTrackingId(this, onGoogleAnalyticsTrackingIdReturned);
        if (p_useActionbar) {
            //Find the parent directory so we can load the common files and assets.
            Tgoma.ApplicationUtils.FileUtils.getParentDirectory(function (p_parentDirectory) {
                console.log("onParentLocationReturned(" + p_parentDirectory + ")");
                p_parentDirectory = p_parentDirectory + "parent/";
                var libariesLoaded = false;
                var parentManifestLoaded = false;
                //Load the shared libraries from the parent directory such as the actionbar and error dialogs.
                Tgoma.ApplicationUtils.FileUtils.loadTgomaLibraries(p_parentDirectory, function () {
                    ActionBar.initialise(p_mainElementId, p_parentDirectory);
                    libariesLoaded = true;
                    TgomaMessaging.Internal.ApplicationManagement.getRaygunApiKey(_this, onRaygunApiKeyReturned);
                    if (parentManifestLoaded && p_callback != null) {
                        p_callback(parentManifest); //Only call the callback if the parents manifest has finished loading as well as the shared libraries.
                    }
                });
                //Load the parents manifest so that we can find the shared assets such as the flags.
                var parentManifest = new Tgoma.ManifestLoader.Manifest(p_parentDirectory + "manifest.xml", function () {
                    console.log("Parent manifest load complete");
                    //Prepend the path to the parents directory to the parents manifest so it can be used as normally.
                    for (var asset in parentManifest.m_assets) {
                        parentManifest.m_assets[asset].m_path = p_parentDirectory + parentManifest.m_assets[asset].m_path;
                    }
                    parentManifestLoaded = true;
                    if (libariesLoaded) {
                        p_callback(parentManifest); //Only call the callback if the parents manifest has finished loading as well as the shared libraries.
                    }
                }, null);
            }, null);
        }
    }
    Tgoma.init = init;
    function onRaygunApiKeyReturned(p_data) {
        console.log("onRaygunApiKeyReturned(", p_data, ")");
        Raygun.init(p_data["apiKey"], {
            disablePulse: false,
            disableAnonymousUserTracking: true
        }).attach();
        if (Tgoma.UserHelper.currentFamilyAccount.m_parseId != null) {
            Raygun.setUser(Tgoma.UserHelper.currentFamilyAccount.m_parseId, false, Tgoma.UserHelper.currentFamilyAccount.m_email, Tgoma.UserHelper.currentFamilyAccount.m_familyName);
        }
        Raygun.saveIfOffline(true);
    }
    function onGoogleAnalyticsTrackingIdReturned(p_data) {
        console.log("onGoogleAnalyticsTrackingIdReturned(", p_data, ")");
        window.analytics.startTrackerWithId(p_data["trackingId"]);
        if (Tgoma.UserHelper.currentFamilyAccount.m_parseId != null) {
            window.analytics.setUserId(Tgoma.UserHelper.currentFamilyAccount.m_parseId);
        }
        // We can now listen for some document events and log them to Google analytics.
        document.addEventListener("onGoalProgress", function (p_event) {
            // If the goal is complete
            if (p_event.percentComplete >= 1.0) {
                // Log a completion event
                window.analytics.trackEvent("Goal", "Complete");
            }
        });
    }
})(Tgoma || (Tgoma = {}));
/// <reference path="Signal.ts" />
/**
*	@desc   	An object that represents a binding between a Signal and a listener function.
*               Released under the MIT license
*				http://millermedeiros.github.com/js-signals/
*
*	@version	1.0 - 7th March 2013
*
*	@author 	Richard Davey, TypeScript conversion
*	@author		Miller Medeiros, JS Signals
*	@author		Robert Penner, AS Signals
*
*	@url		http://www.kiwijs.org
*
*/
var SignalBinding = (function () {
    /**
    * Object that represents a binding between a Signal and a listener function.
    * <br />- <strong>This is an internal constructor and shouldn't be called by regular users.</strong>
    * <br />- inspired by Joa Ebert AS3 SignalBinding and Robert Penner's Slot classes.
    * @author Miller Medeiros
    * @constructor
    * @internal
    * @name SignalBinding
    * @param {Signal} signal Reference to Signal object that listener is currently bound to.
    * @param {Function} listener Handler function bound to the signal.
    * @param {boolean} isOnce If binding should be executed just once.
    * @param {Object} [listenerContext] Context on which listener will be executed (object that should represent the `this` variable inside listener function).
    * @param {Number} [priority] The priority level of the event listener. (default = 0).
    */
    function SignalBinding(signal, listener, isOnce, listenerContext, priority) {
        if (priority === void 0) { priority = 0; }
        /**
        * If binding is active and should be executed.
        * @type boolean
        */
        this.active = true;
        /**
        * Default parameters passed to listener during `Signal.dispatch` and `SignalBinding.execute`. (curried parameters)
        * @type Array|null
        */
        this.params = null;
        this._listener = listener;
        this._isOnce = isOnce;
        this.context = listenerContext;
        this._signal = signal;
        this.priority = priority || 0;
    }
    /**
    * Call listener passing arbitrary parameters.
    * <p>If binding was added using `Signal.addOnce()` it will be automatically removed from signal dispatch queue, this method is used internally for the signal dispatch.</p>
    * @param {Array} [paramsArr] Array of parameters that should be passed to the listener
    * @return {*} Value returned by the listener.
    */
    SignalBinding.prototype.execute = function (paramsArr) {
        var handlerReturn;
        var params;
        if (this.active && !!this._listener) {
            params = this.params ? this.params.concat(paramsArr) : paramsArr;
            handlerReturn = this._listener.apply(this.context, params);
            if (this._isOnce) {
                this.detach();
            }
        }
        return handlerReturn;
    };
    /**
    * Detach binding from signal.
    * - alias to: mySignal.remove(myBinding.getListener());
    * @return {Function|null} Handler function bound to the signal or `null` if binding was previously detached.
    */
    SignalBinding.prototype.detach = function () {
        return this.isBound() ? this._signal.remove(this._listener, this.context) : null;
    };
    /**
    * @return {Boolean} `true` if binding is still bound to the signal and have a listener.
    */
    SignalBinding.prototype.isBound = function () {
        return (!!this._signal && !!this._listener);
    };
    /**
    * @return {boolean} If SignalBinding will only be executed once.
    */
    SignalBinding.prototype.isOnce = function () {
        return this._isOnce;
    };
    /**
    * @return {Function} Handler function bound to the signal.
    */
    SignalBinding.prototype.getListener = function () {
        return this._listener;
    };
    /**
    * @return {Signal} Signal that listener is currently bound to.
    */
    SignalBinding.prototype.getSignal = function () {
        return this._signal;
    };
    /**
    * Delete instance properties
    * @private
    */
    SignalBinding.prototype._destroy = function () {
        delete this._signal;
        delete this._listener;
        delete this.context;
    };
    /**
    * @return {string} String representation of the object.
    */
    SignalBinding.prototype.toString = function () {
        return '[SignalBinding isOnce:' + this._isOnce + ', isBound:' + this.isBound() + ', active:' + this.active + ']';
    };
    return SignalBinding;
})();
/// <reference path="SignalBinding.ts" />
/**
*	@desc       A TypeScript conversion of JS Signals by Miller Medeiros
*               Released under the MIT license
*				http://millermedeiros.github.com/js-signals/
*
*	@version	1.0 - 7th March 2013
*
*	@author 	Richard Davey, TypeScript conversion
*	@author		Miller Medeiros, JS Signals
*	@author		Robert Penner, AS Signals
*
*	@url		http://www.photonstorm.com
*/
/**
* Custom event broadcaster
* <br />- inspired by Robert Penner's AS3 Signals.
* @name Signal
* @author Miller Medeiros
* @constructor
*/
var Signal = (function () {
    function Signal() {
        /**
        * @property _bindings
        * @type Array
        * @private
        */
        this._bindings = [];
        /**
        * @property _prevParams
        * @type Any
        * @private
        */
        this._prevParams = null;
        /**
        * If Signal should keep record of previously dispatched parameters and
        * automatically execute listener during `add()`/`addOnce()` if Signal was
        * already dispatched before.
        * @type boolean
        */
        this.memorize = false;
        /**
        * @type boolean
        * @private
        */
        this._shouldPropagate = true;
        /**
        * If Signal is active and should broadcast events.
        * <p><strong>IMPORTANT:</strong> Setting this property during a dispatch will only affect the next dispatch, if you want to stop the propagation of a signal use `halt()` instead.</p>
        * @type boolean
        */
        this.active = true;
    }
    /**
    * @method validateListener
    * @param {Any} listener
    * @param {Any} fnName
    */
    Signal.prototype.validateListener = function (listener, fnName) {
        if (typeof listener !== 'function') {
            throw new Error('listener is a required param of {fn}() and should be a Function.'.replace('{fn}', fnName));
        }
    };
    /**
    * @param {Function} listener
    * @param {boolean} isOnce
    * @param {Object} [listenerContext]
    * @param {Number} [priority]
    * @return {SignalBinding}
    * @private
    */
    Signal.prototype._registerListener = function (listener, isOnce, listenerContext, priority) {
        var prevIndex = this._indexOfListener(listener, listenerContext);
        var binding;
        if (prevIndex !== -1) {
            binding = this._bindings[prevIndex];
            if (binding.isOnce() !== isOnce) {
                throw new Error('You cannot add' + (isOnce ? '' : 'Once') + '() then add' + (!isOnce ? '' : 'Once') + '() the same listener without removing the relationship first.');
            }
        }
        else {
            binding = new SignalBinding(this, listener, isOnce, listenerContext, priority);
            this._addBinding(binding);
        }
        if (this.memorize && this._prevParams) {
            binding.execute(this._prevParams);
        }
        return binding;
    };
    /**
    * @method _addBinding
    * @param {SignalBinding} binding
    * @private
    */
    Signal.prototype._addBinding = function (binding) {
        //simplified insertion sort
        var n = this._bindings.length;
        do {
            --n;
        } while (this._bindings[n] && binding.priority <= this._bindings[n].priority);
        this._bindings.splice(n + 1, 0, binding);
    };
    /**
    * @method _indexOfListener
    * @param {Function} listener
    * @return {number}
    * @private
    */
    Signal.prototype._indexOfListener = function (listener, context) {
        var n = this._bindings.length;
        var cur;
        while (n--) {
            cur = this._bindings[n];
            if (cur.getListener() === listener && cur.context === context) {
                return n;
            }
        }
        return -1;
    };
    /**
    * Check if listener was attached to Signal.
    * @param {Function} listener
    * @param {Object} [context]
    * @return {boolean} if Signal has the specified listener.
    */
    Signal.prototype.has = function (listener, context) {
        if (context === void 0) { context = null; }
        return this._indexOfListener(listener, context) !== -1;
    };
    /**
    * Add a listener to the signal.
    * @param {Function} listener Signal handler function.
    * @param {Object} [listenerContext] Context on which listener will be executed (object that should represent the `this` variable inside listener function).
    * @param {Number} [priority] The priority level of the event listener. Listeners with higher priority will be executed before listeners with lower priority. Listeners with same priority level will be executed at the same order as they were added. (default = 0)
    * @return {SignalBinding} An Object representing the binding between the Signal and listener.
    */
    Signal.prototype.add = function (listener, listenerContext, priority) {
        if (listenerContext === void 0) { listenerContext = null; }
        if (priority === void 0) { priority = 0; }
        this.validateListener(listener, 'add');
        return this._registerListener(listener, false, listenerContext, priority);
    };
    /**
    * Add listener to the signal that should be removed after first execution (will be executed only once).
    * @param {Function} listener Signal handler function.
    * @param {Object} [listenerContext] Context on which listener will be executed (object that should represent the `this` variable inside listener function).
    * @param {Number} [priority] The priority level of the event listener. Listeners with higher priority will be executed before listeners with lower priority. Listeners with same priority level will be executed at the same order as they were added. (default = 0)
    * @return {SignalBinding} An Object representing the binding between the Signal and listener.
    */
    Signal.prototype.addOnce = function (listener, listenerContext, priority) {
        if (listenerContext === void 0) { listenerContext = null; }
        if (priority === void 0) { priority = 0; }
        this.validateListener(listener, 'addOnce');
        return this._registerListener(listener, true, listenerContext, priority);
    };
    /**
    * Remove a single listener from the dispatch queue.
    * @param {Function} listener Handler function that should be removed.
    * @param {Object} [context] Execution context (since you can add the same handler multiple times if executing in a different context).
    * @return {Function} Listener handler function.
    */
    Signal.prototype.remove = function (listener, context) {
        if (context === void 0) { context = null; }
        this.validateListener(listener, 'remove');
        var i = this._indexOfListener(listener, context);
        if (i !== -1) {
            this._bindings[i]._destroy(); //no reason to a SignalBinding exist if it isn't attached to a signal
            this._bindings.splice(i, 1);
        }
        return listener;
    };
    /**
    * Remove all listeners from the Signal.
    */
    Signal.prototype.removeAll = function () {
        var n = this._bindings.length;
        while (n--) {
            this._bindings[n]._destroy();
        }
        this._bindings.length = 0;
    };
    /**
    * @return {number} Number of listeners attached to the Signal.
    */
    Signal.prototype.getNumListeners = function () {
        return this._bindings.length;
    };
    /**
    * Stop propagation of the event, blocking the dispatch to next listeners on the queue.
    * <p><strong>IMPORTANT:</strong> should be called only during signal dispatch, calling it before/after dispatch won't affect signal broadcast.</p>
    * @see Signal.prototype.disable
    */
    Signal.prototype.halt = function () {
        this._shouldPropagate = false;
    };
    /**
    * Dispatch/Broadcast Signal to all listeners added to the queue.
    * @param {...*} [params] Parameters that should be passed to each handler.
    */
    Signal.prototype.dispatch = function () {
        var paramsArr = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            paramsArr[_i - 0] = arguments[_i];
        }
        if (!this.active) {
            return;
        }
        var n = this._bindings.length;
        var bindings;
        if (this.memorize) {
            this._prevParams = paramsArr;
        }
        if (!n) {
            //should come after memorize
            return;
        }
        bindings = this._bindings.slice(0); //clone array in case add/remove items during dispatch
        this._shouldPropagate = true; //in case `halt` was called before dispatch or during the previous dispatch.
        //execute all callbacks until end of the list or until a callback returns `false` or stops propagation
        //reverse loop since listeners with higher priority will be added at the end of the list
        do {
            n--;
        } while (bindings[n] && this._shouldPropagate && bindings[n].execute(paramsArr) !== false);
    };
    /**
    * Forget memorized arguments.
    * @see Signal.memorize
    */
    Signal.prototype.forget = function () {
        this._prevParams = null;
    };
    /**
    * Remove all bindings from signal and destroy any reference to external objects (destroy Signal object).
    * <p><strong>IMPORTANT:</strong> calling any method on the signal instance after calling dispose will throw errors.</p>
    */
    Signal.prototype.dispose = function () {
        this.removeAll();
        delete this._bindings;
        delete this._prevParams;
    };
    /**
    * @return {string} String representation of the object.
    */
    Signal.prototype.toString = function () {
        return '[Signal active:' + this.active + ' numListeners:' + this.getNumListeners() + ']';
    };
    /**
    * Signals Version Number
    * @property VERSION
    * @type String
    * @const
    */
    Signal.VERSION = '1.0.0';
    return Signal;
})();
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    var ManifestLoader;
    (function (ManifestLoader) {
        var Manifest = (function () {
            /**
            * Constructs a Manifest object. This will take the path to the manifest.xml
            * file and perform an xmlHTTPRequest to load it into
            * memory, then parse the loaded data into groups and assets.
            * @param p_pathToManifestXML relative path to the xml file to load
            * @param p_parseComplete Callback function that is called when the manifest is fully loaded
            * @param p_context A context object for this callback
            */
            function Manifest(p_pathToManifestXML, p_parseComplete, p_context) {
                var _this = this;
                /** A map of tags to ManifestAssets. Used to look up asset paths & details. */
                this.m_assets = {};
                /**
                * A map of group names to arrays of asset tags.
                * The tags are used in conjunction with m_assets to load all the assets in a group.
                */
                this.m_groups = {};
                var _xmlDocument;
                var _xmlHTTPRequest = new XMLHttpRequest();
                _xmlHTTPRequest.open("GET", p_pathToManifestXML);
                _xmlHTTPRequest.send();
                //_xmlHTTPRequest.ontimeout = _xmlHTTPRequest.onabort = _xmlHTTPRequest.onerror = (ev) => { console.log("Loading the manifest failed: " + p_pathToManifestXML); };
                _xmlHTTPRequest.onload = function (ev) {
                    _xmlDocument = _xmlHTTPRequest.responseXML;
                    _this.parseXML(_xmlDocument);
                    p_parseComplete.apply(p_context);
                };
            }
            /**
            * Internal function used to parse the XMLDocument into assets and groups
            * @param _xmlDocument The doc to parse
            */
            Manifest.prototype.parseXML = function (_xmlDocument) {
                // Parse the resources
                var _resourceList = _xmlDocument.querySelectorAll("resource");
                for (var _resourceIndex = 0; _resourceIndex < _resourceList.length; _resourceIndex++) {
                    var node = _resourceList[_resourceIndex];
                    var ass = new ManifestAsset(node);
                    this.m_assets[ass.m_tag] = ass;
                }
                // Parse the groups
                var _groupList = _xmlDocument.querySelectorAll("group");
                for (var _groupIndex = 0; _groupIndex < _groupList.length; _groupIndex++) {
                    // Get the groupname, and add a new collection to the group map.
                    var _groupName = _groupList[_groupIndex].attributes['name'].value;
                    this.m_groups[_groupName] = new Array();
                    // Get the assets within that group
                    var groupAssets = _groupList[_groupIndex].childNodes;
                    this.getAllAssetsInGroup(groupAssets, _groupName);
                }
                var _data = _xmlDocument.getElementsByTagName("data");
                this.m_version = _data[0].attributes.getNamedItem("v").value;
                console.log(this.m_groups);
            };
            Object.defineProperty(Manifest.prototype, "version", {
                /**
                * Returns the manifest's version.
                **/
                get: function () {
                    return this.m_version;
                },
                enumerable: true,
                configurable: true
            });
            /**
            * Internal function used to grab all assets within a group tag
            * @param p_parentGroup The list of nodes to search
            * @param p_groupName The group name to search for
            */
            Manifest.prototype.getAllAssetsInGroup = function (p_parentGroup, p_groupName) {
                var _childNodes;
                for (var _nodeIndex = 0; _nodeIndex < p_parentGroup.length; _nodeIndex++) {
                    var _node = p_parentGroup[_nodeIndex];
                    if (_node.nodeName == "#text") {
                        continue;
                    }
                    else if (_node.nodeName == "reference") {
                        this.addNodeToGroups(_node, p_groupName);
                    }
                    else if (_node.nodeName == "referencedGroup" && _node.hasChildNodes) {
                        this.getAllAssetsInGroup(_node.childNodes, p_groupName);
                    }
                }
            };
            /**
            * Internal function used to add an asset to a group
            * @param p_node The Node to add
            * @param p_groupName The group to add the node to
            */
            Manifest.prototype.addNodeToGroups = function (p_node, p_groupName) {
                var tag = p_node.attributes['tag'].value;
                tag = tag.substring(tag.lastIndexOf("\/") + 1, tag.length);
                this.m_groups[p_groupName].push(tag);
            };
            /**
            * Quick lookup function for getting the path to an asset by its manifest tag.
            * @param p_tag The asset's tag
            * @return Path to the asset as defined in the manifest's path attribute
            */
            Manifest.prototype.getAssetPathByTag = function (p_tag) {
                //return p_tag;
                return this.m_assets[p_tag].m_path;
            };
            return Manifest;
        })();
        ManifestLoader.Manifest = Manifest;
        /**
        * Class describing the structure of an invidual asset in a manifest.
        */
        var ManifestAsset = (function () {
            /**
            * Constructs a ManifestAsset from an XML Node
            * @param p_node Source node
            */
            function ManifestAsset(p_node) {
                /** Tag used to reference this asset **/
                this.m_tag = "";
                /** Path this asset can be found at. This will be a hashed value if the assets have come from the CDN. **/
                this.m_path = "";
                /**
                * Handler that can be used to identify the type of asset this is.
                * i.e. pngs or jpgs would have "image" as their handler
                * and be treated as such by your game.
                **/
                this.m_handler = "";
                /** Size of the asset on disk **/
                this.m_size = 0;
                var _tag = this.getValue(p_node, 'tag');
                this.m_tag = _tag.substring(_tag.lastIndexOf("\/") + 1, _tag.length);
                this.m_path = _tag; //this.getValue(p_node, 'path');
                this.m_handler = this.getValue(p_node, 'handler');
                this.m_size = this.getIntValue(p_node, 'size');
            }
            /** Helper null checker when retrieving the values from the node. */
            ManifestAsset.prototype.getValue = function (p_node, p_value) {
                var val = p_node.attributes[p_value];
                return val == null ? "" : val.value;
            };
            /** Helper for retrieving numerical values from the node. */
            ManifestAsset.prototype.getIntValue = function (p_node, p_value) {
                var val = p_node.attributes[p_value];
                return val == null ? 0 : parseInt(val.value);
            };
            return ManifestAsset;
        })();
        ManifestLoader.ManifestAsset = ManifestAsset;
    })(ManifestLoader = Tgoma.ManifestLoader || (Tgoma.ManifestLoader = {}));
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    var Display;
    (function (Display) {
        /**
        * Helper class used to bundle a bunch of text properties together.
        */
        var TextStyle = (function () {
            function TextStyle() {
                /** Size of text. Defaults to 40. */
                this.size = 40;
                /** Emphasis of text, defaults to bold */
                this.emphasis = "bold";
                /** Text font, defaults to Arial **/
                this.fontFace = "Arial";
                /** Text fill colour, defaults to "#000000" */
                this.fill = "#000000";
                /** Text alignment, defaults to left. */
                this.align = "left";
                /** Outer text stroke, defaults to "#000000" */
                this.stroke = "#000000";
                /** Thickness of outer stroke, defaults to 0 */
                this.strokeThickness = 0;
                /** Word wrap enabled, defaults to true */
                this.wordWrap = true;
                /** Length at which to wrap words to a new line */
                this.wordWrapLength = 100;
            }
            Object.defineProperty(TextStyle.prototype, "font", {
                get: function () {
                    // e.g. "bold 40px Arial"
                    return ((this.emphasis || "") + " " + this.size + "px " + this.fontFace);
                },
                enumerable: true,
                configurable: true
            });
            /**
            * Returns an identical copy of the TextStyle object.
            */
            TextStyle.prototype.clone = function () {
                var _copy = new TextStyle();
                for (var property in this) {
                    _copy[property] = this[property];
                }
                return _copy;
            };
            return TextStyle;
        })();
        Display.TextStyle = TextStyle;
    })(Display = Tgoma.Display || (Tgoma.Display = {}));
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    function loadJSON(jsonFile, p_context, callback) {
        var xobj = new XMLHttpRequest();
        //xobj.overrideMimeType("application/json");
        xobj.open('GET', jsonFile, true);
        xobj.onreadystatechange = function () {
            if (xobj.readyState == 4 && xobj.status == 200) {
                // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
                callback(xobj.responseText, p_context);
            }
        };
        xobj.send(null);
    }
    var MissionManager = (function () {
        /**
         * Constructor. Should only be use for creating the instance. Will throw errors if trying to call it directly.
         */
        function MissionManager() {
            if (MissionManager._sInstance) {
                throw new Error("Error: Instantiation failed: Use MissionManager.getInstance() instead of new.");
            }
            this._mStartedMissionsList = new Array();
            this._mMissionsCompletedThisSession = new Array();
            this._mMissionCompeteSignal = new Signal();
            this._mNewMissionsList = new Array();
        }
        Object.defineProperty(MissionManager, "instance", {
            /**
             * Getter for the MissionManager instance.
             */
            get: function () {
                if (MissionManager._sInstance === null) {
                    MissionManager._sInstance = new MissionManager();
                }
                return MissionManager._sInstance;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Initialise the missions manager by passing in a json file with the setup instructions.
         */
        MissionManager.prototype.initialise = function (p_missionsData) {
            this.parseMissionsJSON(p_missionsData, this);
            //loadJSON(p_missionsFilePath, this, this.parseMissionsJSON);
            Tgoma.UserHelper.userChangedSignal.add(this.onUserChanged, this);
        };
        /**
         * Load the current users active missions from the databse.
         */
        MissionManager.prototype.loadMissions = function () {
            TgomaMessaging.Game.Mission.loadMissions(this, this.onMissionsLoaded);
        };
        /**
         * Event callback for when the active user has changed. It will need to fetch all of the users missions.
         */
        MissionManager.prototype.onUserChanged = function () {
            window.setTimeout(function () { MissionManager.instance.setupUserMissions(); }, 200); //Eww hate this, but need to load the users missions after exiting from the signal or else the callbacks will not work correctly.
        };
        /**
         * Load a list of all of the mission names that a user has started.
         */
        MissionManager.prototype.loadStartedMissionsList = function () {
            TgomaMessaging.Game.Mission.loadStartedMissionsList(this, this.onStartedMissionsListLoaded);
        };
        /**
         * Callback with a list of all of the current users active missions.
         */
        MissionManager.prototype.onMissionsLoaded = function (p_missions, p_context) {
            if (p_context._mActiveMissionsList == null) {
                p_context._mActiveMissionsList = new Array();
            }
            else {
                p_context._mActiveMissionsList.length = 0;
            }
            var missionsList = JSON.parse(p_missions);
            for (var i in missionsList) {
                p_context._mActiveMissionsList.push(Tgoma.Mission.fromObject(missionsList[i]));
            }
        };
        /**
         * Used when swictching accounts, it will set up the user in the mission manager.
         */
        MissionManager.prototype.setupUserMissions = function () {
            if (this._mActiveMissionsList == null) {
                this._mActiveMissionsList = new Array();
            }
            this.saveMissions(); //Save current missions status before loading the new users missions in.
            this.loadStartedMissionsList();
            this.loadMissions();
            while (this._mActiveMissionsList.length < this._mNumberOfActiveMissions) {
                if (!this.startNextMission()) {
                    return;
                }
            }
        };
        /**
         * Callback with a list of the missions the current user has started. Includes completed missions.
         */
        MissionManager.prototype.onStartedMissionsListLoaded = function (p_completedMissions, p_context) {
            p_context._mStartedMissionsList = JSON.parse(p_completedMissions);
        };
        /**
         *  The number of missions a user should have at all times. Set to 0 for game to control missions.
         */
        MissionManager.prototype.setNumberOfActiveMissions = function (p_value) {
            this._mNumberOfActiveMissions = p_value;
        };
        /**
         * Add a mission to the current users list of missions.
         */
        MissionManager.prototype.addMission = function (p_missionTag, p_objectives, p_description, p_rewardType, p_rewardAmount) {
            //var missionID: number = SpringfreeMessaging.Game.createMission(p_missionTag, p_description);
            //SpringfreeMessaging.Game.setMissionReward(missionID, p_rewardType, p_rewardAmount)
            //for (var i in p_objectives)
            //{
            //    var goalID: number = SpringfreeMessaging.Game.addMissionGoal(missionID, p_objectives[i].goalType, p_objectives[i].target, p_objectives[i].goalSubType);
            //    p_objectives[i].goalID = goalID;
            //}
            return 0; //missionID;
        };
        /**
         * Save the missions from memory into the database.
         */
        MissionManager.prototype.saveMissions = function () {
            for (var i in this._mActiveMissionsList) {
                this._mActiveMissionsList[i].save();
            }
        };
        /**
         * Starts the next mission from the missions list, will return false if there are no more avaible missions.
         */
        MissionManager.prototype.startNextMission = function () {
            for (var i = 0; i < this._mMissionsList.length; i++) {
                if (this._mStartedMissionsList.indexOf(this._mMissionsList[i].missionTag) < 0) {
                    var mission = this._mMissionsList[i];
                    var missionID = this.addMission(mission.missionTag, mission.objectives, mission.description, mission.rewardType, mission.rewardQuantity);
                    mission.missionID = missionID;
                    this._mActiveMissionsList.push(mission);
                    this._mStartedMissionsList.push(mission.missionTag);
                    this._mNewMissionsList.push(mission);
                    return true;
                }
            }
            return false;
        };
        /**
         * Updates the missions list to enusure there are enough missions.
         */
        MissionManager.prototype.updateMissionList = function () {
            this._mNewMissionsList.length = 0;
            while (this._mActiveMissionsList.length < this._mNumberOfActiveMissions) {
                if (!this.startNextMission()) {
                    return;
                }
            }
            console.log(this._mNewMissionsList);
        };
        Object.defineProperty(MissionManager.prototype, "activeMissionsList", {
            /**
             * Getter for an array of active missions.
             */
            get: function () {
                return this._mActiveMissionsList;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Used to set up the mission manager using the missions json file. Includes setting up all of the predetermined missions.
         */
        MissionManager.prototype.parseMissionsJSON = function (p_missions, p_context) {
            var missionSettings = p_missions;
            p_context.setNumberOfActiveMissions(missionSettings.numberOfActiveMissions);
            if (this._mMissionsList == null) {
                p_context._mMissionsList = new Array();
            }
            else {
                p_context._mMissionsList.length = 0;
            }
            for (var i in missionSettings.Missions) {
                p_context._mMissionsList.push(new Tgoma.Mission(missionSettings.Missions[i].Tag, missionSettings.Missions[i].Description, missionSettings.Missions[i].MissionType, missionSettings.Missions[i].Target, missionSettings.Missions[i].MissionSubType, missionSettings.Missions[i].RewardType, missionSettings.Missions[i].RewardQuantity, missionSettings.Missions[i].Image));
            }
        };
        /**
         * Add a mission to the mission complete on this level list.
         */
        MissionManager.prototype.addCompletedMission = function (p_mission) {
            console.log(this._mMissionCompeteSignal.getNumListeners().toString());
            this._mMissionsCompletedThisSession.push(p_mission);
            this._mMissionCompeteSignal.dispatch(p_mission);
            console.log(this._mMissionsCompletedThisSession);
        };
        Object.defineProperty(MissionManager.prototype, "newMissionsList", {
            get: function () {
                return this._mNewMissionsList;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Remove this missions from the missions completed this session array from the active missions list.
         */
        MissionManager.prototype.removeAllCompleteMissions = function () {
            console.log(this._mMissionsCompletedThisSession);
            console.log("Removing completed missions");
            for (var i = this._mActiveMissionsList.length - 1; i >= 0; i--) {
                console.log("checking mission", this._mActiveMissionsList[i]);
                if (this._mActiveMissionsList[i].status == Tgoma.Mission.MISSION_COMPLETE) {
                    console.log("Removing mission");
                    this._mActiveMissionsList.splice(i, 1);
                }
            }
            console.log(this._mActiveMissionsList);
        };
        /**
         * Remove a mission from the active missions list.
         */
        MissionManager.prototype.removeActiveMission = function (p_mission) {
            this._mActiveMissionsList.splice(this._mActiveMissionsList.indexOf(p_mission), 1);
        };
        Object.defineProperty(MissionManager.prototype, "missionsCompletedThisSession", {
            get: function () {
                return this._mMissionsCompletedThisSession;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MissionManager.prototype, "missionCompleteSignal", {
            get: function () {
                return this._mMissionCompeteSignal;
            },
            enumerable: true,
            configurable: true
        });
        MissionManager._sInstance = null;
        return MissionManager;
    })();
    Tgoma.MissionManager = MissionManager;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    var Mission = (function () {
        function Mission(p_tag, p_description, p_missionType, p_target, p_subType, p_rewardType, p_rewardQuantity, p_image) {
            this.missionTag = p_tag;
            this.description = p_description;
            this.objectives = new Array();
            this.objectives.push(new Tgoma.MissionGoal(p_missionType, p_target, p_subType, 0, this));
            this.rewardType = p_rewardType;
            this.rewardQuantity = p_rewardQuantity;
            this.image = p_image;
            this.status = Mission.MISSION_ACTIVE;
        }
        Mission.fromObject = function (jsonObject) {
            var mission = new Mission();
            mission.missionID = jsonObject["missionID"];
            mission.missionTag = jsonObject["missionTag"];
            mission.userName = jsonObject["userName"];
            mission.gameID = jsonObject["gameID"];
            mission.rewardType = jsonObject["rewardType"];
            mission.rewardQuantity = jsonObject["rewardQuantity"];
            mission.startDate = jsonObject["startDate"]; //TODO: turn into dates not string.
            mission.endDate = jsonObject["endDate"]; //TODO: turn into dates not strings.
            mission.status = jsonObject["status"];
            mission.description = jsonObject["description"];
            mission.objectives = new Array();
            var objectives = jsonObject["objectives"];
            for (var i in objectives) {
                mission.objectives.push(Tgoma.MissionGoal.fromObject(objectives[i], mission));
            }
            return mission;
        };
        Mission.prototype.save = function () {
            for (var i in this.objectives) {
                TgomaMessaging.Game.Mission.saveMissionGoal(this.objectives[i].goalID, this.objectives[i].progress);
            }
            TgomaMessaging.Game.Mission.saveMission(this.missionID, this.status, this.description);
        };
        Object.defineProperty(Mission.prototype, "completed", {
            get: function () {
                for (var i in this.objectives) {
                    if (!this.objectives[i].completed) {
                        return false;
                    }
                }
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Mission.prototype.goalCompleted = function () {
            if (this.completed && this.status == Mission.MISSION_ACTIVE) {
                console.log("MissionComplete");
                Tgoma.MissionManager.instance.addCompletedMission(this);
                this.status = Mission.MISSION_COMPLETE;
                this.endDate = new Date().toTimeString();
                this.save();
                console.log(this);
            }
        };
        //TODO: Make sure these match up with java.
        Mission.MISSION_ACTIVE = 1;
        Mission.MISSION_COMPLETE = 2;
        return Mission;
    })();
    Tgoma.Mission = Mission;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    var MissionGoal = (function () {
        function MissionGoal(p_goalType, p_target, p_subType, p_goalID, p_parentMission) {
            if (p_subType === void 0) { p_subType = 0; }
            if (p_goalID === void 0) { p_goalID = 0; }
            this.GoalType = p_goalType;
            this.Target = p_target;
            this.GoalID = p_goalID;
            this.parentMission = p_parentMission;
            this.Progress = 0;
            this.SubType = p_subType;
        }
        Object.defineProperty(MissionGoal.prototype, "goalType", {
            get: function () {
                return this.GoalType;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MissionGoal.prototype, "target", {
            get: function () {
                return this.Target;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MissionGoal.prototype, "goalID", {
            get: function () {
                return this.GoalID;
            },
            set: function (p_value) {
                this.GoalID = p_value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MissionGoal.prototype, "goalSubType", {
            get: function () {
                return this.SubType;
            },
            enumerable: true,
            configurable: true
        });
        MissionGoal.prototype.addOne = function (p_subType) {
            console.log("adding one");
            if (this.SubType == undefined || this.SubType == null || p_subType == this.SubType || this.SubType < 0) {
                this.progress++;
            }
        };
        MissionGoal.prototype.setMax = function (p_value, p_subType) {
            if (this.SubType == undefined || this.SubType == null || p_subType == this.SubType || this.SubType < 0) {
                if (p_value > this.Progress) {
                    this.progress = p_value;
                }
            }
        };
        MissionGoal.prototype.addProgress = function (p_progress, p_subType) {
            if (this.SubType == undefined || this.SubType == null || p_subType == this.SubType || this.SubType < 0) {
                this.progress += p_progress;
            }
        };
        MissionGoal.prototype.checkGreaterThanSub = function (p_value) {
            if (p_value >= this.SubType) {
                this.addOne(this.SubType);
            }
        };
        MissionGoal.prototype.checkLessThanSub = function (p_value) {
            if (p_value <= this.SubType) {
                this.addOne(this.SubType);
            }
        };
        Object.defineProperty(MissionGoal.prototype, "progress", {
            get: function () {
                return this.Progress;
            },
            set: function (p_value) {
                if (this.completed)
                    return;
                this.Progress = p_value;
                console.log("Adding progress " + this.Progress + " of " + this.target);
                if (this.completed) {
                    this.Progress = this.Target;
                    this.parentMission.goalCompleted();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MissionGoal.prototype, "completed", {
            get: function () {
                return this.Progress >= this.Target;
            },
            enumerable: true,
            configurable: true
        });
        MissionGoal.fromObject = function (jsonObject, p_mission) {
            var goal = new MissionGoal(jsonObject.GoalType, jsonObject.Target, jsonObject.SubType, jsonObject.GoalID, p_mission);
            goal.Progress = jsonObject.Progress;
            console.log("Current progress = " + goal.progress);
            return goal;
        };
        return MissionGoal;
    })();
    Tgoma.MissionGoal = MissionGoal;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    /**
    * Class representing a goal object
    */
    var Goal = (function () {
        function Goal() {
        }
        return Goal;
    })();
    Tgoma.Goal = Goal;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    /**
    * This class contains information about the current Family Account and current Family Member.
    * This should be used to get information such as the current family member's username or the
    * current family account's country.
    */
    var UserHelper = (function () {
        function UserHelper() {
        }
        /**
        * Initialises the UserHelper by adding document events
        * and fetching the current family account & member.
        */
        UserHelper.init = function () {
            document.addEventListener("onFamilyMemberChanged", UserHelper.onGetFamilyMemberChanged, false);
            document.addEventListener("onFamilyMemberUpdated", UserHelper.onFamilyMemberUpdated, false);
            document.addEventListener("onFamilyLoginReturned", UserHelper.onFamilyLoginReturned, false);
            document.addEventListener("onFamilyCreateReturned", UserHelper.onFamilyCreateReturned, false);
            document.addEventListener("onFamilyAccountUpdated", UserHelper.onFamilyAccountUpdated, false);
            TgomaMessaging.Internal.UserManagement.getFamilyAccount(this, UserHelper.onGetFamilyAccountReturned);
            TgomaMessaging.Internal.UserManagement.getFamilyMember(this, UserHelper.onGetFamilyMemberReturned);
        };
        /**
        * Cleans up the document events
        */
        UserHelper.destroy = function () {
            document.removeEventListener("onFamilyMemberChanged", UserHelper.onGetFamilyMemberChanged);
            document.removeEventListener("onFamilyMemberUpdated", UserHelper.onFamilyMemberUpdated);
            document.removeEventListener("onFamilyLoginReturned", UserHelper.onFamilyLoginReturned);
            document.removeEventListener("onFamilyCreateReturned", UserHelper.onFamilyCreateReturned);
            document.removeEventListener("onFamilyAccountUpdated", UserHelper.onFamilyAccountUpdated);
        };
        /**
        * Called when the family account returns and saves off family account information
        * to currentFamilyAccount. See the getter.
        */
        UserHelper.onGetFamilyAccountReturned = function (p_familyAccountJSON) {
            console.log("onGetFamilyAccountReturned()");
            UserHelper.m_currentFamilyAccount = new FamilyAccount(p_familyAccountJSON);
        };
        /**
        * Called when the family account returns and saves off family account information
        * to currentFamilyAccount. See the getter.
        */
        UserHelper.onFamilyAccountUpdated = function (p_event) {
            console.log("onGetFamilyAccountReturned()");
            if (p_event.success) {
                UserHelper.m_currentFamilyAccount = new FamilyAccount(p_event.family);
            }
        };
        /**
        * Called when the family member returns and saves off family account information
        * to currentFamilyAccount. See the getter.
        */
        UserHelper.onGetFamilyMemberReturned = function (p_familyMemberJSON) {
            console.log("onGetFamilyMemberReturned()");
            UserHelper.m_currentFamilyMember = new FamilyMember(p_familyMemberJSON);
            UserHelper.m_userChangedSignal.dispatch(UserHelper.m_currentFamilyMember);
        };
        /**
        * Called when the family member changes and saves off family member information
        * to currentFamilyMember See the getter.
        */
        UserHelper.onGetFamilyMemberChanged = function (p_event) {
            console.log("onGetFamilyMemberReturned()");
            UserHelper.m_currentFamilyMember = new FamilyMember(p_event.familyMember);
            UserHelper.m_userChangedSignal.dispatch(UserHelper.m_currentFamilyMember);
        };
        /**
        * Called when the family member is updated, it then saves off family member information
        * to currentFamilyMember See the getter.
        */
        UserHelper.onFamilyMemberUpdated = function (p_event) {
            console.log("onGetFamilyMemberReturned()");
            UserHelper.m_currentFamilyMember = new FamilyMember(p_event.familyMember);
            UserHelper.m_userUpdatedSignal.dispatch(UserHelper.m_currentFamilyMember);
        };
        Object.defineProperty(UserHelper, "currentUser", {
            /**
            * Getter for the current user. Deprecated, use currentFamilyMember() instead
            */
            get: function () {
                return UserHelper.currentFamilyMemberName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UserHelper, "currentFamilyMemberName", {
            /**
            * @return the current family member's name.
            */
            get: function () {
                return UserHelper.m_currentFamilyMember.m_name;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UserHelper, "userChangedSignal", {
            /**
            * Getter for the signal. Listen to this when you want to know when the family member changes!
            */
            get: function () {
                return UserHelper.m_userChangedSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UserHelper, "userUpdatedSignal", {
            /**
            * Getter for the signal. Listen to this when you want to know when the family member is updated!
            */
            get: function () {
                return UserHelper.m_userUpdatedSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UserHelper, "currentFamilyMember", {
            /**
            * Getter for the current family member's details. Useful for grabbing their name or weight.
            */
            get: function () {
                return UserHelper.m_currentFamilyMember;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UserHelper, "currentFamilyAccount", {
            /**
            * Getter for the current family account's details. Useful for grabbing their country etc.
            */
            get: function () {
                return UserHelper.m_currentFamilyAccount;
            },
            enumerable: true,
            configurable: true
        });
        /**
        * Handles a user creation event, and dispatches its signal.
        */
        UserHelper.onUserCreationComplete = function (p_outcome, p_displayNameOrError) {
            console.log("onUserCreationComplete(" + p_outcome + ", " + p_displayNameOrError + ")");
            UserHelper.m_userCreationCompleteSignal.dispatch(p_outcome, p_displayNameOrError);
        };
        Object.defineProperty(UserHelper, "userCreationCompleteSignal", {
            /**
            * A Signal used when creating online users, that will be fired when a response is
            * received from the server.
            */
            get: function () {
                console.log("userCreationCompleteSignal()");
                return UserHelper.m_userCreationCompleteSignal;
            },
            enumerable: true,
            configurable: true
        });
        /**
        * Handles a family creation event, and dispatches its signal.
        */
        UserHelper.onFamilyCreateReturned = function (p_event) {
            console.log("onFamilyCreateReturned(" + p_event.succeeded + ", " + p_event.errorOrId + ")");
            UserHelper.m_familyCreationReturnedSignal.dispatch(p_event.succeeded, p_event.errorOrId);
        };
        /**
        * Handles a family login event, and dispatches its signal.
        */
        UserHelper.onFamilyLoginReturned = function (p_event) {
            console.log("onFamilyLoginReturned(" + p_event.succeeded + ", " + p_event.errorOrId + ")");
            UserHelper.m_familyLoginReturnedSignal.dispatch(p_event.succeeded, p_event.errorOrId);
        };
        Object.defineProperty(UserHelper, "familyCreationReturnedSignal", {
            /**
            * Getter for the family creation signal. Games shouldn't need to listen to this.
            */
            get: function () {
                console.log("familyCreationReturnedSignal()");
                return UserHelper.m_familyCreationReturnedSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UserHelper, "familyLoginReturnedSignal", {
            /**
            * Getter for the family login signal. Games shouldn't need to listen to this,
            */
            get: function () {
                console.log("familyCreationReturnedSignal()");
                return UserHelper.m_familyLoginReturnedSignal;
            },
            enumerable: true,
            configurable: true
        });
        /** Signal that dispatches when the user changes, containing information on the new user */
        UserHelper.m_userChangedSignal = new Signal();
        /** Signal that dispatches when the user is updated, containing updated information on the user */
        UserHelper.m_userUpdatedSignal = new Signal();
        /** Signal that dispatches when a new user is created */
        UserHelper.m_userCreationCompleteSignal = new Signal();
        /** Signal that dispatches when a family has been successfully created. */
        UserHelper.m_familyCreationReturnedSignal = new Signal();
        /** Signal that dispatches when a family account has successfully logged in. */
        UserHelper.m_familyLoginReturnedSignal = new Signal();
        return UserHelper;
    })();
    Tgoma.UserHelper = UserHelper;
    /**
    * A Family Account contains information about which country / region a
    * family is from, the family name, and their internationalisation unit preferences.
    */
    var FamilyAccount = (function () {
        /**
        * Creates a FamilyAccount from a JSON string
        * @param p_jsonObject String received from native containing family account information.
        */
        function FamilyAccount(p_jsonObject) {
            this.m_familyName = p_jsonObject["familyName"];
            this.m_country = p_jsonObject["country"];
            this.m_region = p_jsonObject["region"];
            this.m_email = p_jsonObject["email"];
            this.m_dateIsUSFormat = p_jsonObject["dateIsUSFormat"];
            this.m_energyIsUSFormat = p_jsonObject["energyIsUSFormat"];
            this.m_unitsAreUSFormat = p_jsonObject["unitsAreUSFormat"];
            this.m_parseId = p_jsonObject["parseId"];
            this.m_serialNumber = p_jsonObject["serialNumber"];
        }
        return FamilyAccount;
    })();
    Tgoma.FamilyAccount = FamilyAccount;
    var FamilyMember = (function () {
        /**
        * Creates a FamilyMember from a JSON string
        * @param p_jsonObject String received from native containing family member information.
        */
        function FamilyMember(p_jsonObject) {
            this.m_name = p_jsonObject["userName"];
            this.m_weight = p_jsonObject["weight"];
            this.m_isVerified = p_jsonObject["isVerified"];
        }
        return FamilyMember;
    })();
    Tgoma.FamilyMember = FamilyMember;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    /**
    * Class to help with Trampoline connectivity status.
    */
    var ConnectionHelper = (function () {
        function ConnectionHelper() {
        }
        /**
        * Initialises the document events for connection status and fetches
        * the current connection status from the native layer.
        */
        ConnectionHelper.init = function () {
            document.addEventListener("onConnectionStatusChanged", ConnectionHelper.onConnectionStatusChanged, false);
            TgomaMessaging.Misc.getIsConnectedToTrampoline(this, ConnectionHelper.onConnectionStatusReturned);
        };
        /**
        * Removes the connection status events from the document.
        */
        ConnectionHelper.destroy = function () {
            document.removeEventListener("onConnectionStatusChanged", ConnectionHelper.onConnectionStatusChanged);
        };
        /**
        * Handles connection status change events. This will update the local value and dispatch a change event to all listeners.
        */
        ConnectionHelper.onConnectionStatusChanged = function (p_event) {
            console.log("onConnectionStatusChanged(" + p_event.connectionStatus + ")");
            ConnectionHelper._mConnected = p_event.connectionStatus;
            ConnectionHelper.m_connectionChangedSignal.dispatch(ConnectionHelper._mConnected);
        };
        /**
        * Handles the initial connection status response.
        */
        ConnectionHelper.onConnectionStatusReturned = function (p_connectionStatus) {
            console.log("onConnectionStatusChanged(" + p_connectionStatus + ")");
            ConnectionHelper._mConnected = p_connectionStatus;
            ConnectionHelper.m_connectionChangedSignal.dispatch(ConnectionHelper._mConnected);
        };
        Object.defineProperty(ConnectionHelper, "isConnected", {
            /**
            * Getter for current connection status.
            */
            get: function () {
                return ConnectionHelper._mConnected;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ConnectionHelper, "connectionSignal", {
            /**
            * Getter for connection change signal.
            */
            get: function () {
                return ConnectionHelper.m_connectionChangedSignal;
            },
            enumerable: true,
            configurable: true
        });
        /** Cached copy of connection status for quick access. */
        ConnectionHelper._mConnected = false;
        /** Signal that can be subscribed to to get connection change events. */
        ConnectionHelper.m_connectionChangedSignal = new Signal();
        return ConnectionHelper;
    })();
    Tgoma.ConnectionHelper = ConnectionHelper;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    /** This class assists with trampoline events that are passed from the native layer.
    * They are dispatched as document events, which this class listens to.
    * Developers can choose whether to listen to the document events manually
    * and filter out the relevant events based on their eventIds (not recommended),
    * or subscribe to particular signals on this class which will do the filtering automatically.
    *
    * The signals are broken up into 2 groups - location events, which
    * deal exclusively with the location of a jump event, and generic messages,
    * which contain all other types of trampoline event (such as impact, depart, calories etc)
    *
    * Location events are either:
    * 1. UnitCircle - Normalised [0, 1] with relation to a play area that will offer
    * a consistent experience across multiple trampoline models & sizes (e.g. 100cm)
    * 2. MaximumArea - Normalised [0, 1] with relation to the maximum play area of
    * the model currently being used.
    *
    * This class also handles trampoline details changing, which occurs when the
    * user reconfigures their trampoline such as by changing the firmware version
    * or model type. This is not relevant information for games, however.
    **/
    var TrampolineHelper = (function () {
        function TrampolineHelper() {
        }
        /**
        * Initialises the TrampolineHelper and attaches document listeners.
        */
        TrampolineHelper.init = function () {
            document.addEventListener("onGenericTrampolineMessage", TrampolineHelper.onGenericMessage, false);
            document.addEventListener("onPolarLocationMessage", TrampolineHelper.onPolarLocationMessage, false);
            document.addEventListener("onTrampolineDetailsUpdated", TrampolineHelper.onTrampolineDetailsUpdated, false);
        };
        /**
        * Cleans up the TrampolineHelper and removes document listeners.
        */
        TrampolineHelper.destroy = function () {
            document.removeEventListener("onGenericTrampolineMessage", TrampolineHelper.onGenericMessage);
            document.removeEventListener("onPolarLocationMessage", TrampolineHelper.onPolarLocationMessage);
            document.removeEventListener("onTrampolineDetailsUpdated", TrampolineHelper.onTrampolineDetailsUpdated);
        };
        /**
        * Handles a large number of trampoline events and filters them to individually subscribed listeners.
        */
        TrampolineHelper.onGenericMessage = function (p_event) {
            console.log("[TrampolineHelper] [TS] onGenericMessage(" + p_event.eventId + ", " + p_event.payload + ")");
            if (TrampolineHelper.callbacks[p_event.eventId] != null) {
                TrampolineHelper.callbacks[p_event.eventId].dispatch(p_event.payload);
            }
        };
        /**
        * Handles the polar location events and sends the magnitude and angle of the jump to subscribed listeners.
        */
        TrampolineHelper.onPolarLocationMessage = function (p_event) {
            console.log("[TrampolineHelper] [TS] onPolarLocationMessage(" + p_event.magnitude + ", " + p_event.angle + ")");
            TrampolineHelper.m_gameAreaNormValue = p_event.unitCircleNorm;
            TrampolineHelper.m_maximumAreaNormValue = p_event.maximisedAreaNorm;
            TrampolineHelper.m_polarLocationDeprecatedSignal.dispatch(p_event.magnitude, p_event.angle);
            //Convert the brad angle that goes clockwise and starts at 12 o'clock to a more usable angle starting at 3 going anti-clockwise
            var angle_rads = (Math.PI / 128) * ((64 - p_event.angle) % 256);
            var unitCircleRadius = Math.min(p_event.magnitude / p_event.unitCircleNorm, 1);
            var maxAreaNormRadius = Math.min(p_event.magnitude / p_event.maximisedAreaNorm, 1);
            TrampolineHelper.m_polarLocationUnitCircleNormSignal.dispatch(unitCircleRadius, angle_rads);
            TrampolineHelper.m_polarLocationMaximumAreaNormSignal.dispatch(maxAreaNormRadius, angle_rads);
        };
        /**
        * Handles the trampoline's details being updated and dispatches to listeners.
        */
        TrampolineHelper.onTrampolineDetailsUpdated = function (p_event) {
            console.log("[TrampolineHelper] [TS] onTrampolineDetailsUpdated(" + p_event.trampolineModel + ", " + p_event.hardwareVersion + ", " + p_event.firmwareVersion + ", " + p_event.hardwareId + ")");
            TrampolineHelper.m_trampolineDetailsChangedSignal.dispatch(p_event.hardwareId, p_event.trampolineModel, p_event.hardwareVersion, p_event.firmwareVersion);
            if (p_event.firmwareVersion != "" && p_event.firmwareVersion != "unset") {
                window.analytics.trackEvent("FirmwareVersion", p_event.firmwareVersion);
            }
        };
        /**
        * Used to subscribe to a particular type of trampoline event.
        * @param p_eventID The ID of the event that should call your callback. The list of event IDs can be found in TgomaMessaging.Enum
        * @param p_callback Your callback function. This receives a numerical payload and the context you originally subscribed with.
        * @param p_callbackContext The context you wish your callback to receive.
        */
        TrampolineHelper.subscribeToMessage = function (p_eventID, p_callback, p_callbackContext) {
            console.log("[TrampolineHelper] [TS] subscribeToMessage(" + p_eventID + ")");
            if (TrampolineHelper.callbacks[p_eventID] == null) {
                TrampolineHelper.callbacks[p_eventID] = new Signal();
            }
            TrampolineHelper.callbacks[p_eventID].add(p_callback, p_callbackContext);
        };
        /**
        * Used to unsubscribe from a particular type of trampoline event.
        * @param p_eventID The event ID you wish to be unsubscribed from.
        * @param p_callback The p_callback you originally passed in.
        * @param p_callbackContext The context you originally passed in.
        */
        TrampolineHelper.unsubscribeFromMessage = function (p_eventID, p_callback, p_callbackContext) {
            if (TrampolineHelper.callbacks[p_eventID] != null) {
                TrampolineHelper.callbacks[p_eventID].remove(p_callback, p_callbackContext);
            }
        };
        Object.defineProperty(TrampolineHelper, "polarLocationMessageDeprecatedSignal", {
            /**
            * @DEPRECATED
            * Getter for the polar location signal. This is what you subscribe to if you want to receive magnitude & angle events in Bradians clockwise from 12 O'clock. This has been deprecated
            * and you should use polarLocationGameCircleNormalSignal, or polarLocationMaximumAreaSignal instead.
            */
            get: function () {
                return TrampolineHelper.m_polarLocationDeprecatedSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TrampolineHelper, "polarLocationGameCircleNormalSignal", {
            /**
            * Getter for the polar location signal. This is what you subscribe to if you want to receive magnitude & angle events. The distance has been normalised between 0 and 1 to give a consistent physical game area across trampolines. The angle is in Radians.
            */
            get: function () {
                return TrampolineHelper.m_polarLocationUnitCircleNormSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TrampolineHelper, "polarLocationMaximumAreaSignal", {
            /**
            * Getter for the polar location signal. This is what you subscribe to if you want to receive magnitude & angle events. The distance has been normalised between 0 and 1 to give a consistent position based on full size of the trampoline. The angle is in Radians.
            */
            get: function () {
                return TrampolineHelper.m_polarLocationMaximumAreaNormSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TrampolineHelper, "trampolineDetailsChangedSignal", {
            /**
            * Getter for the tramp details changing signal. This is what you'd subscribe
            * to if you want to be notified of changes to the controller on the trampoline.
            */
            get: function () {
                return TrampolineHelper.m_trampolineDetailsChangedSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TrampolineHelper, "trampolineGameAreaNormal", {
            /**
             * Returns the game area normal value, used to normalise the magnitude of a bounce based on a game area of a trampoline.
             * This value may change between jumps on some trampolines.
             */
            get: function () {
                return TrampolineHelper.m_gameAreaNormValue;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TrampolineHelper, "trampolineMaximumAreaNormal", {
            /**
             * Returns the game area normal value, used to normalise the magnitude of a bounce based on the full size of a trampoline.
             * This value may change between jumps on some trampolines.
             */
            get: function () {
                return TrampolineHelper.m_maximumAreaNormValue;
            },
            enumerable: true,
            configurable: true
        });
        /** The signal used to dispatch polar location events. Deprecated - this does not provide a consistent or accurate experience across trampoline models. */
        TrampolineHelper.m_polarLocationDeprecatedSignal = new Signal();
        /** The signal for dispatching the normalised location **/
        TrampolineHelper.m_polarLocationUnitCircleNormSignal = new Signal();
        /** The signal for dispatching the full location **/
        TrampolineHelper.m_polarLocationMaximumAreaNormSignal = new Signal();
        /** A collection of signals used to dispatch all other types of event. */
        TrampolineHelper.callbacks = {};
        /** Signal used to dispatch new trampoline details when they're updated by the user. */
        TrampolineHelper.m_trampolineDetailsChangedSignal = new Signal();
        /** Constant used to normalise the game area **/
        TrampolineHelper.m_gameAreaNormValue = 0;
        /** Maximum area of the current trampoline model **/
        TrampolineHelper.m_maximumAreaNormValue = 0;
        return TrampolineHelper;
    })();
    Tgoma.TrampolineHelper = TrampolineHelper;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    /**
    * Class to handle errors that may exist on the controller - loose cables, authentication issues etc.
    */
    var ErrorHelper = (function () {
        function ErrorHelper() {
        }
        /**
        * Initialises the document events to receive controller errors from native.
        */
        ErrorHelper.init = function () {
            document.addEventListener("onControllerError", ErrorHelper.onControllerError, false);
        };
        /**
        * Cleans up the document events.
        */
        ErrorHelper.destroy = function () {
            document.removeEventListener("onControllerError", ErrorHelper.onControllerError);
        };
        /**
        * Handles controller error events, updating the local value and dispatching a signal.
        */
        ErrorHelper.onControllerError = function (p_event) {
            console.log("onControllerError(" + p_event.errorField1 + ", " + +p_event.errorField2 + ", " + p_event.errorField3 + ")");
            ErrorHelper.m_errorEnum1 = p_event.errorField1;
            ErrorHelper.m_errorEnum2 = p_event.errorField2;
            ErrorHelper.m_errorEnum3 = p_event.errorField3;
            ErrorHelper.m_errorSignal.dispatch(p_event.errorField1, p_event.errorField2, p_event.errorField3);
        };
        Object.defineProperty(ErrorHelper, "errorSignal", {
            /**
            * Getter for signal.
            */
            get: function () {
                return ErrorHelper.m_errorSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ErrorHelper, "currentErrorField1", {
            /**
            * Getter for current error enum.
            */
            get: function () {
                return ErrorHelper.m_errorEnum1;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ErrorHelper, "currentErrorField2", {
            /**
            * Getter for current error enum.
            */
            get: function () {
                return ErrorHelper.m_errorEnum2;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ErrorHelper, "currentErrorField3", {
            /**
            * Getter for current error enum.
            */
            get: function () {
                return ErrorHelper.m_errorEnum3;
            },
            enumerable: true,
            configurable: true
        });
        /** Signal that will dispatch controller errors. */
        ErrorHelper.m_errorSignal = new Signal();
        /** Error enum representing the errors currently being experienced by the controller. */
        ErrorHelper.m_errorEnum1 = 0;
        ErrorHelper.m_errorEnum2 = 0;
        ErrorHelper.m_errorEnum3 = 0;
        return ErrorHelper;
    })();
    Tgoma.ErrorHelper = ErrorHelper;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    /**
    * Class to help deal with event leaderboard calls. As these are returned asynchronously from the cloud, so helpers are helpful here.
    */
    var EventHelper = (function () {
        function EventHelper() {
        }
        /**
        * Initialises the helper and attaches listeners to document events from native.
        */
        EventHelper.init = function () {
            document.addEventListener("onEventLeaderboardReturned", EventHelper.onEventLeaderboardReturned, false);
            document.addEventListener("onEventLeaderboardUserRankReturned", EventHelper.onEventLeaderboardUserRankReturned, false);
        };
        /**
        * Cleans up and removes the document events.
        */
        EventHelper.destroy = function () {
            document.removeEventListener("onEventLeaderboardReturned", EventHelper.onEventLeaderboardReturned);
            document.removeEventListener("onEventLeaderboardUserRankReturned", EventHelper.onEventLeaderboardUserRankReturned);
        };
        /**
        * Handles leaderboard data for events and will dispatch the JSON blob to any listeners.
        */
        EventHelper.onEventLeaderboardReturned = function (p_event) {
            console.log("onEventLeaderboardReturned(" + p_event.eventLeaderboard + ")");
            EventHelper.m_eventLeaderboardReturnedSignal.dispatch(JSON.parse(p_event.eventLeaderboard));
        };
        /**
        * Handles individual leaderboard data for a single user's contribution to events and will dispatch the JSON blob to any listeners.
        */
        EventHelper.onEventLeaderboardUserRankReturned = function (p_event) {
            console.log("onEventLeaderboardUserRankReturned(" + p_event.eventLeaderboardUserRank + ")");
            EventHelper.m_eventLeaderboardUserRankReturnedSignal.dispatch(JSON.parse(p_event.eventLeaderboardUserRank));
        };
        Object.defineProperty(EventHelper, "eventLeaderboardReturnedSignal", {
            /**
            * Getter for the event leaderboard signal
            */
            get: function () {
                return EventHelper.m_eventLeaderboardReturnedSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventHelper, "userRankReturnedSignal", {
            /**
            * Getter for the individual user contribution signal
            */
            get: function () {
                return EventHelper.m_eventLeaderboardUserRankReturnedSignal;
            },
            enumerable: true,
            configurable: true
        });
        EventHelper.m_eventLeaderboardReturnedSignal = new Signal();
        EventHelper.m_eventLeaderboardUserRankReturnedSignal = new Signal();
        return EventHelper;
    })();
    Tgoma.EventHelper = EventHelper;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    /**
    * Class to assist with highscore fetching
    */
    var HighScoreHelper = (function () {
        function HighScoreHelper() {
        }
        /**
        * Initialises the helper and attaches listeners to document events from native.
        */
        HighScoreHelper.init = function () {
            document.addEventListener("onHighScoreReturned", HighScoreHelper.onHighScoreReturned, false);
            document.addEventListener("onUserRankReturned", HighScoreHelper.onUserRankReturned, false);
            document.addEventListener("onHighscoreError", HighScoreHelper.onHighScoreError, false);
        };
        /**
        * Retrieves either the local or cloud leaderboard for the specified level,
        * depending on the parameter passed in. LeaderboardData object will be dispatched
        * to anything listening to the HighScoreHelper.leaderboardReturnedSignal.
        * @param p_local    retrieve local or cloud data
        * @param p_level    the level to retrieve data for
        **/
        HighScoreHelper.getLeaderboard = function (p_local, p_level) {
            HighScoreHelper.m_leaderboard = null;
            HighScoreHelper.m_userRank = null;
            if (p_local) {
                TgomaMessaging.Game.Leaderboard.getLocalLeaderboardDataForLevel(p_level);
            }
            else {
                TgomaMessaging.Game.Leaderboard.getLeaderboardDataForLevel(p_level);
            }
        };
        /**
        * Cleans up and removes the document events.
        */
        HighScoreHelper.destroy = function () {
            document.removeEventListener("onHighScoreReturned", HighScoreHelper.onHighScoreReturned);
            document.removeEventListener("onUserRankReturned", HighScoreHelper.onUserRankReturned);
            document.removeEventListener("onHighscoreError", HighScoreHelper.onHighScoreError);
        };
        /**
        * Handles the high scores returning and dispatches the signal event
        */
        HighScoreHelper.onHighScoreReturned = function (p_event) {
            console.log("onHighScoreReturned(" + p_event.highScores + ")");
            HighScoreHelper.m_leaderboard = JSON.parse(p_event.highScores);
            if (HighScoreHelper.m_userRank != null) {
                // If we were waiting, then this is the second piece of the puzzle.
                HighScoreHelper.m_leaderboardReturnedSignal.dispatch(new LeaderboardData(HighScoreHelper.m_leaderboard, HighScoreHelper.m_userRank));
            }
        };
        /**
        * Handles the high scores returning an error and dispatches a signal event
        */
        HighScoreHelper.onHighScoreError = function (p_event) {
            console.log("onHighScoreError(" + p_event.errorMessage + "," + p_event.errorId + ")");
            HighScoreHelper.m_leaderboardErrorSignal.dispatch(p_event.errorMessage, p_event.errorId);
        };
        /**
        * Handles the individual user rank returning and dispatches the signal event
        */
        HighScoreHelper.onUserRankReturned = function (p_event) {
            console.log("onUserRankReturned(" + p_event.userRank + ")");
            HighScoreHelper.m_userRank = JSON.parse(p_event.userRank);
            if (HighScoreHelper.m_leaderboard != null) {
                // If we were waiting, then this is the second piece of the puzzle.
                HighScoreHelper.m_leaderboardReturnedSignal.dispatch(new LeaderboardData(HighScoreHelper.m_leaderboard, HighScoreHelper.m_userRank));
            }
        };
        Object.defineProperty(HighScoreHelper, "leaderboardReturnedSignal", {
            /**
            * Getter for highscore signal
            */
            get: function () {
                return HighScoreHelper.m_leaderboardReturnedSignal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HighScoreHelper, "leadboardErrorSignal", {
            get: function () {
                return HighScoreHelper.m_leaderboardErrorSignal;
            },
            enumerable: true,
            configurable: true
        });
        HighScoreHelper.m_leaderboard = null;
        HighScoreHelper.m_userRank = null;
        /** Signal used for dispatching high score table results */
        HighScoreHelper.m_leaderboardReturnedSignal = new Signal();
        HighScoreHelper.m_leaderboardErrorSignal = new Signal();
        return HighScoreHelper;
    })();
    Tgoma.HighScoreHelper = HighScoreHelper;
    var LeaderboardData = (function () {
        function LeaderboardData(p_leaderboard, p_userRank) {
            this.leaderboard = p_leaderboard;
            this.userRank = p_userRank;
        }
        return LeaderboardData;
    })();
    Tgoma.LeaderboardData = LeaderboardData;
    /**
    * Object representing the form high scores will be returned in.
    */
    var HighScoreData = (function () {
        function HighScoreData(p_name, p_rank, p_score, p_userId, p_country) {
            this.m_name = p_name;
            this.m_rank = p_rank;
            this.m_score = p_score;
            this.m_userId = p_userId;
            this.m_country = p_country;
        }
        Object.defineProperty(HighScoreData.prototype, "name", {
            get: function () {
                return this.m_name;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HighScoreData.prototype, "userId", {
            get: function () {
                return this.m_userId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HighScoreData.prototype, "rank", {
            get: function () {
                return this.m_rank;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HighScoreData.prototype, "score", {
            get: function () {
                return this.m_score;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HighScoreData.prototype, "country", {
            get: function () {
                return this.m_country;
            },
            enumerable: true,
            configurable: true
        });
        return HighScoreData;
    })();
    Tgoma.HighScoreData = HighScoreData;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    /**
    * Class to help with battery status / level events received from native.
    */
    var BatteryHelper = (function () {
        function BatteryHelper() {
        }
        /**
        * Initialises the BatteryHelper which subscribes to battery status events.
        */
        BatteryHelper.init = function () {
            document.addEventListener("onBatteryStatus", BatteryHelper.onBatteryStatus, false);
        };
        /**
        * Removed the listeners for battery status events.
        */
        BatteryHelper.destroy = function () {
            document.removeEventListener("onBatteryStatus", BatteryHelper.onBatteryStatus);
        };
        /**
        * This function is called whenever the native code dispatches a battery status event.
        * It will relay this event to anything listening to the battery signal.
        */
        BatteryHelper.onBatteryStatus = function (p_event) {
            console.log("onBatteryStatus(" + p_event.status + ")");
            BatteryHelper.m_batteryHelper.dispatch(p_event.status);
        };
        Object.defineProperty(BatteryHelper, "batterySignal", {
            /**
            * Getter for battery signal.
            */
            get: function () {
                return BatteryHelper.m_batteryHelper;
            },
            enumerable: true,
            configurable: true
        });
        /** Battery signal used to dispatch battery events */
        BatteryHelper.m_batteryHelper = new Signal();
        return BatteryHelper;
    })();
    Tgoma.BatteryHelper = BatteryHelper;
})(Tgoma || (Tgoma = {}));
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    /**
    * A Class of static utility functions.
    **/
    var Utils = (function () {
        function Utils() {
        }
        /**
        * Vertically centers an object between 2 values
        * @param p_object The object to center. Needs a position.y value
        * @param p_top The upper bound of the area to center within
        * @param p_bottom The lower bound of the area to center within
        * @param p_proportion The weighting to use, defaults to 0.5 for center. i.e. 0.1 will skew the 'centering' towards the top.
        **/
        Utils.centerWithinBoundsV = function (p_object, p_top, p_bottom, p_proportion) {
            if (p_proportion === void 0) { p_proportion = 0.5; }
            p_object.position.y = Math.round(p_top + ((p_bottom - p_top) * p_proportion) - (p_object.height / 2));
        };
        /**
        * Horizontally centers an object between 2 values
        * @param p_object The object to center. Needs a position.x value
        * @param p_left The left bound of the area to center within
        * @param p_right The right bound of the area to center within
        * @param p_proportion The weighting to use, defaults to 0.5 for center. i.e. 0.1 will skew the 'centering' towards the left.
        **/
        Utils.centerWithinBoundsH = function (p_object, p_left, p_right, p_proportion) {
            if (p_proportion === void 0) { p_proportion = 0.5; }
            p_object.position.x = Math.round(p_left + ((p_right - p_left) * p_proportion) - (p_object.width / 2));
        };
        /**
        * Returns a rounded random number between a range
        * @param p_min The minimum number i.e. 2
        * @param p_max The maximum number i.e. 10
        * @return Rounded randomised number
        **/
        Utils.roundedRandomInRange = function (p_min, p_max) {
            return Math.floor(Utils.randomInRange(p_min, p_max));
        };
        /**
        * Returns a random number between a range of values
        * @param p_min The minimum number i.e. 2.1
        * @param p_max The maximum number i.e. 10.5
        * @return Randomised value
        **/
        Utils.randomInRange = function (p_min, p_max) {
            return (Math.random() * (p_max - p_min + 1)) + p_min;
        };
        /**
        * Converts metres to feet
        * @param m_metres Value in metres
        * @return Value in feet
        **/
        Utils.convertMetresToFeet = function (m_metres) {
            return m_metres * 3.28084;
        };
        /**
        * Converts metres to feet
        * @param m_metres Value in metres
        * @return Value in feet
        **/
        Utils.convertFeetToMetres = function (m_feet) {
            return m_feet * 0.3048;
        };
        /**
        * Converts kilograms to pounds
        * @param m_kilograms Value in kg
        * @return Value in pounds
        **/
        Utils.convertKilogramsToPounds = function (m_kilograms) {
            return m_kilograms * 2.20462;
        };
        /**
        * Converts pounds to kilograms
        * @param m_pounds Value in pounds
        * @return Value in kilograms
        **/
        Utils.convertPoundsToKilograms = function (m_pounds) {
            return m_pounds * 0.453592;
        };
        /**
        * Converts calories to kilojoules
        * @param m_calories Value in calories
        * @return Value in kilojoules
        **/
        Utils.convertCaloriesToKiloJoules = function (m_calories) {
            return m_calories * 4.184;
        };
        /**
        * Converts kilojoules to calories
        * @param m_kilojoules Value in kilojoules
        * @return Value in calories
        **/
        Utils.convertKiloJoulesToCalories = function (m_kilojoules) {
            return m_kilojoules * 0.239;
        };
        return Utils;
    })();
    Tgoma.Utils = Utils;
})(Tgoma || (Tgoma = {}));
///<reference path='Tgoma.ts'/>
///<reference path='Signal.ts'/>
///<reference path='SignalBinding.ts'/>
///<reference path='ApplicationUtils.ts'/>
///<reference path='jquery.d.ts'/>
///<reference path='jqueryui.d.ts'/>
///<reference path='TgomaMessaging.d.ts'/>
///<reference path='ManifestLoader.ts'/>
///<reference path='IAssetLoader.ts'/>
///<reference path='TextStyle.ts'/>
///<reference path='MissionManager.ts'/>
///<reference path='Mission.ts'/>
///<reference path='MissionGoal.ts'/>
///<reference path='Goal.ts'/>
///<reference path='UserHelper.ts'/>
///<reference path='ConnectionHelper.ts'/>
///<reference path='TrampolineHelper.ts'/>
///<reference path='ErrorHelper.ts'/>
///<reference path='EventHelper.ts'/>
///<reference path='HighScoreHelper.ts'/>
///<reference path='BatteryHelper.ts'/>
///<reference path='Utils.ts'/>
///<reference path='references.ts'/>
var Tgoma;
(function (Tgoma) {
    var ApplicationUtils;
    (function (ApplicationUtils) {
        /**
        * A collection of functions that deal with loading manifests and common files.
        */
        var FileUtils = (function () {
            function FileUtils() {
            }
            /**
            * Generic helper function for loading a manifest xml.
            * @param p_path Path to xml document
            * @param p_callback onload callback that contains the manifest as responseXML
            */
            FileUtils.loadManifest = function (p_path, p_callback) {
                console.log("loadManifest(" + p_path + ")");
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", p_path, false);
                xmlhttp.onload = p_callback;
                xmlhttp.send();
            };
            /**
            * Critical function that loads the cordova libraries.
            * This function will also create a div on the document for the error dialog.
            * @param p_document The document element onto which
            * the cordova scripts and error dialog div will be loaded.
            **/
            FileUtils.loadCordovaLibraries = function (p_document) {
                if (DeviceUtils.isMobile.Android()) {
                    console.log("Running on Android");
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = '/android_asset/www/cordova.js';
                    p_document.head.appendChild(script);
                }
                else if (DeviceUtils.isMobile.iOS()) {
                    console.log("Running on iOS");
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = '../cordova.js';
                    p_document.head.appendChild(script);
                }
                else {
                    console.log("Running on Desktop");
                }
                // While we're here and we have a reference to the document, 
                // we should append the div where errors will be placed.
                var _errorElement = p_document.createElement("div");
                _errorElement.setAttribute("class", "error-dialog");
                p_document.body.appendChild(_errorElement);
            };
            /**
              * Loads the actionbar files, the error dialogs, and the packaged jquery/jqueryUI from the parent app.
              * @param p_pathToParentApp    Path to the parent app directory. This is used to grab some common files such as JQuery. This can be found via ApplicationUtils.FileUtils.getParentDirectory();
              * @param p_callback   Function called when all libraries have finished loading.
              */
            FileUtils.loadTgomaLibraries = function (p_pathToParentApp, p_callback) {
                if (p_callback === void 0) { p_callback = null; }
                var errorDialogScript = document.createElement('script');
                errorDialogScript.type = 'text/javascript';
                errorDialogScript.src = p_pathToParentApp + "errorDialog.js";
                errorDialogScript.onload = ApplicationUtils.FileUtils.onLibraryLoaded;
                document.head.appendChild(errorDialogScript);
                FileUtils.m_librariesToLoad++;
                var actionbarScript = document.createElement('script');
                actionbarScript.type = 'text/javascript';
                actionbarScript.src = p_pathToParentApp + "actionbar.js";
                actionbarScript.onload = ApplicationUtils.FileUtils.onLibraryLoaded;
                document.head.appendChild(actionbarScript);
                FileUtils.m_librariesToLoad++;
                var jqueryScript = document.createElement('script');
                jqueryScript.type = 'text/javascript';
                jqueryScript.src = p_pathToParentApp + "jquery-1.10.2.min.js";
                jqueryScript.onload = ApplicationUtils.FileUtils.onLibraryLoaded;
                document.head.appendChild(jqueryScript);
                FileUtils.m_librariesToLoad++;
                var jqueryUIScript = document.createElement('script');
                jqueryUIScript.type = 'text/javascript';
                jqueryUIScript.src = p_pathToParentApp + "jquery-ui.min.js";
                jqueryUIScript.onload = ApplicationUtils.FileUtils.onLibraryLoaded;
                document.head.appendChild(jqueryUIScript);
                FileUtils.m_librariesToLoad++;
                var jqueryCSS = document.createElement("link");
                jqueryCSS.rel = 'stylesheet';
                jqueryCSS.href = p_pathToParentApp + "jquery-ui.css";
                jqueryCSS.type = 'text/css';
                jqueryCSS.onload = ApplicationUtils.FileUtils.onLibraryLoaded;
                document.head.appendChild(jqueryCSS);
                FileUtils.m_librariesToLoad++;
                var raygunScript = document.createElement('script');
                raygunScript.type = 'text/javascript';
                raygunScript.src = p_pathToParentApp + "raygun.js";
                raygunScript.onload = ApplicationUtils.FileUtils.onLibraryLoaded;
                document.head.appendChild(raygunScript);
                FileUtils.m_librariesToLoad++;
                FileUtils.m_librariesCallback = p_callback;
            };
            /**
            * Callback for a single library load completing. Fires the main callback when all are loaded.
            */
            FileUtils.onLibraryLoaded = function (p_event) {
                console.log("onLibraryLoaded()");
                if (++FileUtils.m_librariesLoaded == FileUtils.m_librariesToLoad && FileUtils.m_librariesCallback != null) {
                    console.log("onLibraryLoaded finished()");
                    FileUtils.m_librariesCallback();
                }
            };
            /**
            * Generic helper function for finding the path to an asset
            * @param p_xml The xml doc to search
            * @param p_tag The asset tag to look for
            * @return _filePath The path to the asset
            */
            FileUtils.getPathByTagInXML = function (p_xml, p_tag) {
                console.log("getPathByTagInXML(" + p_tag + ")");
                return p_tag;
                var _filePath = "";
                if (p_xml == null) {
                    return _filePath;
                }
                var _resources = p_xml.getElementsByTagName("resource");
                var _index;
                for (_index = 0; _index < _resources.length; _index++) {
                    var _tag = _resources[_index].attributes.getNamedItem("tag").value;
                    if (_tag == p_tag) {
                        _filePath = _resources[_index].attributes.getNamedItem("path").value;
                        break;
                    }
                }
                return _filePath;
            };
            /**
            * Helper function for finding the internal parent directory. For internal use.
            */
            FileUtils.getParentDirectory = function (p_callback, p_context) {
                TgomaMessaging.Misc.getParentDirectory(p_context, p_callback);
            };
            FileUtils.m_librariesLoaded = 0;
            FileUtils.m_librariesToLoad = 0;
            return FileUtils;
        })();
        ApplicationUtils.FileUtils = FileUtils;
        /**
        * Class for getting information about the current device
        */
        var DeviceUtils = (function () {
            function DeviceUtils() {
            }
            /**
            * Object that can be used to determine current platform. Usage:
            * if (isMobile.iOS()) { ... }
            */
            DeviceUtils.isMobile = {
                Android: function () {
                    return navigator.userAgent.match(/Android/i);
                },
                iOS: function () {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
                Windows: function () {
                    return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
                },
                any: function () {
                    return (DeviceUtils.isMobile.Android() || DeviceUtils.isMobile.iOS() || DeviceUtils.isMobile.Windows());
                }
            };
            return DeviceUtils;
        })();
        ApplicationUtils.DeviceUtils = DeviceUtils;
    })(ApplicationUtils = Tgoma.ApplicationUtils || (Tgoma.ApplicationUtils = {}));
})(Tgoma || (Tgoma = {}));
